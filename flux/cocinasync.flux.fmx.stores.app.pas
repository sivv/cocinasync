unit cocinasync.flux.fmx.stores.app;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,

  FMX.Platform,

  cocinasync.flux.fmx.actions.ui,
  cocinasync.flux.store;

type
  TAppStore = class(TBaseStore)
  private
    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
  protected
    procedure FinishedLaunching; virtual;
    procedure BecameActive; virtual;
    procedure WillBecomeInactive; virtual;
    procedure EnteredBackground; virtual;
    procedure WillBecomeForeground; virtual;
    procedure WillTerminate; virtual;
    procedure LowMemory; virtual;  // iOS Only
    procedure TimeChange; virtual;
    procedure OpenURL; virtual;  // iOS Only
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

{ TBaseAppStore }

procedure TAppStore.BecameActive;
begin
  TAppBecameActiveAction.Send;
end;

constructor TAppStore.Create;
var
  aFMXApplicationEventService: IFMXApplicationEventService;
begin
  inherited;
  if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService, IInterface(aFMXApplicationEventService)) then
    aFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent);
end;

destructor TAppStore.Destroy;
var
  aFMXApplicationEventService: IFMXApplicationEventService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService, IInterface(aFMXApplicationEventService)) then
    aFMXApplicationEventService.SetApplicationEventHandler(nil);
  inherited;
end;

procedure TAppStore.EnteredBackground;
begin
  TAppEnteredBackgroundAction.Send;
end;

procedure TAppStore.FinishedLaunching;
begin
  TAppFinishedLaunchingAction.Send;
end;

function TAppStore.HandleAppEvent(AAppEvent: TApplicationEvent;
  AContext: TObject): Boolean;
begin
  case AAppEvent of
    TApplicationEvent.FinishedLaunching:
      FinishedLaunching;
    TApplicationEvent.BecameActive:
      BecameActive;
    TApplicationEvent.WillBecomeInactive:
      WillBecomeInactive;
    TApplicationEvent.EnteredBackground:
      EnteredBackground;
    TApplicationEvent.WillBecomeForeground:
      WillBecomeForeground;
    TApplicationEvent.WillTerminate:
      WillTerminate;
    TApplicationEvent.LowMemory:
      LowMemory;
    TApplicationEvent.TimeChange:
      TimeChange;
    TApplicationEvent.OpenURL:
      OpenURL;
  end;
end;

procedure TAppStore.LowMemory;
begin
  TAppLowMemoryWarningAction.Send;
end;

procedure TAppStore.OpenURL;
begin
  TAppOpenURLAction.Send;
end;

procedure TAppStore.TimeChange;
begin
  TAppTimeChangeAction.Send;
end;

procedure TAppStore.WillBecomeForeground;
begin
  TAppWillBecomeForegroundAction.Send;
end;

procedure TAppStore.WillBecomeInactive;
begin
  TAppWillBecomeInactiveAction.Send;
end;

procedure TAppStore.WillTerminate;
begin
  TAppWillTerminateAction.Send;
end;

end.
