unit cocinasync.flux.fmx.stores.screen;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,

  FMX.Types,

  cocinasync.flux.fmx.actions.ui,
  cocinasync.flux.store;

type
  TScreenStore = class(TBaseStore)
  private
    FOrientationChangedHandle : THandle;
    FWidth : Single;
    FHeight : Single;
    FOrientation : TScreenOrientation;
    procedure OnOrientationChanged(Sender : TObject; Action : TOrientationChangedAction);
    procedure DoOrientationChanged(const Sender: TObject; const M: TMessage);
    procedure TriggerOrientationAction;
  public
    constructor Create; override;
    destructor Destroy; override;

    property Orientation : TScreenOrientation read FOrientation;
    property Width : Single read FWidth;
    property Height : Single read FHeight;
  end;

implementation

uses
  FMX.Platform,
  FMX.Forms,

  cocinasync.flux;

{ TScreenStore }

constructor TScreenStore.Create;
begin
  inherited;
  Flux.Register<TOrientationChangedAction>(Self, OnOrientationChanged);

  FOrientationChangedHandle := TMessageManager.DefaultManager.SubscribeToMessage(TOrientationChangedMessage, DoOrientationChanged);
  TriggerOrientationAction;
end;

procedure TScreenStore.OnOrientationChanged(Sender: TObject; Action: TOrientationChangedAction);
begin
  FOrientation := Action.Orientation;
  FWidth := Screen.Width;
  FHeight := Screen.Height;
end;

procedure TScreenStore.TriggerOrientationAction;
var
  s: IFMXScreenService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXScreenService, s) then
  begin
    TOrientationChangedAction.Send(s.GetScreenOrientation);
  end;
end;

destructor TScreenStore.Destroy;
begin
  TMessageManager.DefaultManager.Unsubscribe(TOrientationChangedMessage, FOrientationChangedHandle);

  inherited;
end;

procedure TScreenStore.DoOrientationChanged(const Sender: TObject; const M: TMessage);
begin
  TriggerOrientationAction;
end;

end.
