unit cocinasync.flux.fmx.actions.ui;

interface

uses
  System.SysUtils,
  System.Classes,

  FMX.Types,
  FMX.Forms,

  cocinasync.flux.action;

type
  TOrientationChangedAction = class(TIntegralAction)
  private
    FOrientation: TScreenOrientation;
  public
    class procedure Send(AOrientation : TScreenOrientation);
    property Orientation : TScreenOrientation read FOrientation;
  end;

  TShortMessageAction = class(TIntegralAction)
  private
    FMsg : string;
  public
    class procedure Send(const AMsg : string);
    property Msg : string read FMsg;
  end;

  TNotificationAction = class(TIntegralAction)
  private
    FMsg : string;
  public
    class procedure Send(const AMsg : string);
    property Msg : string read FMsg;
  end;

  TAppBecameActiveAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppEnteredBackgroundAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppFinishedLaunchingAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppLowMemoryWarningAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppOpenURLAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppTimeChangeAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppWillBecomeForegroundAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppWillBecomeInactiveAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TAppWillTerminateAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

  TShowBusyAction = class(TIntegralAction)
  private
    FDockParent : TCustomForm;
  public
    class procedure Send(DockParent : TCustomForm);
    property DockParent : TCustomForm read FDockParent;
  end;

  THideBusyAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

implementation

{ TOrientationChangedAction }

class procedure TOrientationChangedAction.Send(AOrientation : TScreenOrientation);
var
  a : TOrientationChangedAction;
begin
  a := TOrientationChangedAction(inherited New(TOrientationChangedAction));
  a.FOrientation := AOrientation;
  a.Dispatch;
end;

{ TAppBecameActiveAction }

class procedure TAppBecameActiveAction.Send;
begin
  TAppBecameActiveAction(inherited New(TAppBecameActiveAction)).Send;
end;

{ TAppEnteredBackgroundAction }

class procedure TAppEnteredBackgroundAction.Send;
begin
  TAppEnteredBackgroundAction(inherited New(TAppEnteredBackgroundAction)).Send;
end;

{ TAppFinishedLaunchingAction }

class procedure TAppFinishedLaunchingAction.Send;
begin
  TAppFinishedLaunchingAction(inherited New(TAppFinishedLaunchingAction)).Send;
end;

{ TAppLowMemoryWarningAction }

class procedure TAppLowMemoryWarningAction.Send;
begin
  TAppLowMemoryWarningAction(inherited New(TAppLowMemoryWarningAction)).Send;
end;

{ TAppOpenURLAction }

class procedure TAppOpenURLAction.Send;
begin
  TAppOpenURLAction(inherited New(TAppOpenURLAction)).Send;
end;

{ TAppTimeChange }

class procedure TAppTimeChangeAction.Send;
begin
  TAppTimeChangeAction(inherited New(TAppTimeChangeAction)).Send;
end;

{ TAppWillBecomeInactive }

class procedure TAppWillBecomeInactiveAction.Send;
begin
  TAppWillBecomeInactiveAction(inherited New(TAppWillBecomeInactiveAction)).Send;
end;

{ TAppWillTerminate }

class procedure TAppWillTerminateAction.Send;
begin
  TAppWillTerminateAction(inherited New(TAppWillTerminateAction)).Send;
end;

{ TAppWillBecomeForeground }

class procedure TAppWillBecomeForegroundAction.Send;
begin
  TAppWillBecomeForegroundAction(inherited New(TAppWillBecomeForegroundAction)).Send;
end;

{ TShortMessageAction }

class procedure TShortMessageAction.Send(const AMsg: string);
var
  a : TShortMessageAction;
begin
  a := TShortMessageAction(inherited New(TShortMessageAction));
  a.FMsg := AMsg;
  a.Dispatch;
end;

{ TNotificationAction }

class procedure TNotificationAction.Send(const AMsg: string);
var
  a : TNotificationAction;
begin
  a := TNotificationAction(inherited New(TNotificationAction));
  a.FMsg := AMsg;
  a.Dispatch;
end;

{ THideBusyAction }

class procedure THideBusyAction.Send;
begin
  THideBusyAction(inherited New(THideBusyAction)).Dispatch;
end;

{ TShowBusyAction }

class procedure TShowBusyAction.Send(DockParent : TCustomForm);
var
  act : TShowBusyAction;
begin
  act := TShowBusyAction(inherited New(TShowBusyAction));
  act.FDockParent := DockParent;
  act.Dispatch;
end;

end.
