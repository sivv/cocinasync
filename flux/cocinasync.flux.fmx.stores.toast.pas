unit cocinasync.flux.fmx.stores.toast;

interface

uses
  System.SysUtils,
  System.Classes,

  cocinasync.flux.fmx.actions.ui,
  cocinasync.collections,
  cocinasync.flux.store;

type
  IToaster = interface
    procedure MakeToast(const Msg : string);
  end;

  TToastStore = class(TBaseStore)
  strict private
    class var FData: TToastStore;
  strict private
    FLastMessage: string;
    FToaster : IToaster;
    procedure OnShortMessage(Sender : TObject; Action : TShortMessageAction);
  public
    class property Data : TToastStore read FData;
    class constructor Create;
    class destructor Destroy;

    constructor Create; override;
    destructor Destroy; override;

    property LastMessage : string read FLastMessage;
  end;

implementation

uses
  {$IFDEF ANDROID}
  cocinasync.flux.fmx.stores.toast.android,
  {$ELSE}
  cocinasync.flux.fmx.stores.toast.fake,
  {$ENDIF}

  cocinasync.flux;

{ TToastStore }

class constructor TToastStore.Create;
begin
  FData := TToastStore.Create;
end;

constructor TToastStore.Create;
begin
  inherited;
  FToaster := TToaster.Create;

  Flux.Register<TShortMessageAction>(Self, OnShortMessage);
end;

class destructor TToastStore.Destroy;
begin
  FData.Free;
end;

destructor TToastStore.Destroy;
begin
  FToaster := nil;
  inherited;
end;

procedure TToastStore.OnShortMessage(Sender: TObject;
  Action: TShortMessageAction);
begin
  FLastMessage := Action.Msg;
  FToaster.MakeToast(Action.Msg);
end;

end.
