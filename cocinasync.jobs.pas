unit cocinasync.jobs;

interface

uses
  System.SysUtils,
  System.SyncObjs,
  System.Generics.Collections,
  Cocinasync.Collections,
  cocinasync.monitor;

var
  CreateCount : Int64;
  UseCount : Int64;

type
  TEventClass = class of TEvent;


  EJobExecutionFailure = class(Exception)

  end;

  IJob = interface
    procedure SetupJob;
    procedure ExecuteJob;
    procedure FinishJob;
    function Wait(Timeout : Cardinal = INFINITE) : boolean; overload;
    procedure Wait(var Completed : boolean; Timeout : Cardinal = INFINITE); overload;
    procedure RaiseExceptionIfExists;
    function ExecutionTime : Cardinal;
    function Name : string;
    procedure Done;
  end;

  IJob<T> = interface(IJob)
    function Result : T;
  end;

  TJobQueue = class(TQueue<IJob>)
  public
    function WaitForAll(Timeout : Cardinal = INFINITE) : boolean; inline;
    procedure Abort;
  end;

  TJobQueue<T> = class(TQueue<IJob<T>>)
  public
    function WaitForAll(Timeout : Cardinal = INFINITE) : boolean; inline;
    procedure Abort;
  end;

  TJobHandler = reference to procedure(const Job : IJob);
  IJobs = interface
    procedure Call(const DoIt : TProc); overload;
    function Queue(const DoIt : TProc) : IJob; overload;
    function Queue(const Job : IJob) : IJob; overload;
    procedure WaitForAll(Timeout : Cardinal = INFINITE);
    function Name : string;
    function Count : Integer;
    function RunnerCount : Integer;
  end;

  TJobException = record
    Clss : String;
    Msg : String;
    Addr : Pointer;
    Triggered : boolean;
    CallStack : String;
    class function Init : TJobException; static;
    procedure Update(E: Exception);
    procedure RaiseExceptionIfExists(const JobName : string);
  end;

  TDefaultJob<T> = class(TInterfacedObject, IJob, IJob<T>)
  strict private
    class var FPool : TStack<IJob<T>>;
  private
    FExecutionTime : Cardinal;
    FProcToExecute : TProc;
    FFuncToExecute : TFunc<T>;
    FEvent : TEvent;
    FResult : T;
    FException : TJobException;
    FName : string;
    FDone : boolean;
    procedure SetEvent; inline;
  public
    class constructor Create;
    class destructor Destroy;
    class property Pool : TStack<IJob<T>> read FPool;
  public
    constructor Create(ProcToExecute : TProc; FuncToExecute : TFunc<T>; AName : string; DefaultDone : boolean); reintroduce; virtual;
    destructor Destroy; override;

    procedure Reset(AName: string; ProcToExecute: TProc; FuncToExecute: TFunc<T>; DefaultDone : boolean);
    procedure Done;
    procedure ExecuteJob; inline;
    procedure SetupJob; inline;
    procedure FinishJob; inline;
    function Wait(Timeout : Cardinal = INFINITE) : boolean; overload; inline;
    procedure Wait(var Completed : boolean; Timeout : Cardinal = INFINITE); overload; inline;
    function Result : T; inline;
    procedure RaiseExceptionIfExists;
    function ExecutionTime : Cardinal;
    function Name : string;
  end;

  TEachIdxIterator<T> = reference to procedure(const Value : T; Index : integer);
  TEachProgress = reference to procedure(const Processed : integer; Total : integer);

  TJobManager = class sealed
  private
    class var FMonitor : IJobMonitor;
    class var FEventClass : TEventClass;

    class function LaunchJobForEach<T>(const Value : T; OnValue : TValueHandler<T>) : IJob; inline;
  public
    class procedure RegisterMonitor(Monitor : IJobMonitor);
    class procedure UnregisterMonitor(Monitor : IJobMonitor);
    class procedure ShowMonitor;
    class procedure HideMonitor;
    class function CreateJobs(RunnerCount : Cardinal = 0; MaxJobs : Integer = 4096; const Name : string = '') : IJobs;
    class function Job(const AJob : TProc; const AName : string = ''; DefaultDone : boolean = false) : IJob; overload; inline;
    class function Job<T>(const AJob : TFunc<T>; const AName : string = '') : IJob<T>; overload; inline;

    class procedure Call(const AJob: TProc; AJobs : IJobs = nil); overload; inline;
    class procedure Call<T>(const WithValue: T; const AJob: TValueHandler<T>; AJobs : IJobs = nil); overload; inline;

    class function Execute(const AJob : TProc; AJobs : IJobs = nil; const AName : string = '') : IJob; overload; inline;
    class function Execute<T>(const AJob : TFunc<T>; AJobs : IJobs = nil; const AName : string = '') : IJob<T>; overload; inline;
    class function Execute(const AJob : TProc; AQueue : TJobQueue; AJobs : IJobs = nil; const AName : string = '') : IJob; overload; inline;
    class function Execute<T>(const AJob : TFunc<T>; AQueue : TJobQueue<T>; AJobs : IJobs = nil; const AName : string = '') : IJob<T>; overload; inline;

    class procedure ProcessQueue<T>(AQueue : TQueue<T>; AJob : TValueHandler<T>;
      WaitForItems : boolean = false; AOnWait : TProc<boolean> = nil); inline;
    class property EventClass : TEventClass read FEventClass write FEventClass;

    class procedure Each(From, Through : Integer; OnValue : TValueHandler<Integer>); overload; inline;
    class procedure Each(From, Through : Int64; OnValue : TValueHandler<Int64>); overload; inline;
    class procedure Each(From, Through : Cardinal; OnValue : TValueHandler<Cardinal>); overload; inline;
    class procedure Each(From, Through : UInt64; OnValue : TValueHandler<UInt64>); overload; inline;

    class procedure Each(From, Through, Stride : Integer; OnValue : TValueHandler<Integer>); overload; inline;
    class procedure Each(From, Through, Stride : Int64; OnValue : TValueHandler<Int64>); overload; inline;
    class procedure Each(From, Through, Stride : Cardinal; OnValue : TValueHandler<Cardinal>); overload; inline;
    class procedure Each(From, Through, Stride : UInt64; OnValue : TValueHandler<UInt64>); overload; inline;

    class procedure Each<T>(Values : TArray<T>; OnValue : TValueHandler<T>); overload; inline;
    class procedure Each<T>(Values : TArray<T>; OnValue : TValueHandler<T>; OnProgress : TEachProgress); overload; inline;
    class procedure Each<T>(Values : TArray<T>; OnValue : TValueHandler<T>; Wait : boolean); overload; inline;
    class procedure Each<T>(Values : TArray<T>; OnValue : TValueHandler<T>; OnProgress : TEachProgress; Wait : boolean); overload; inline;

    class procedure Each<T>(Values : TArray<T>; From, Through : Integer; OnValue : TValueHandler<T>); overload; inline;
    class procedure Each<T>(Values : TArray<T>; From, Through : Integer; OnValue : TValueHandler<T>; OnProgress : TEachProgress); overload; inline;
    class procedure Each<T>(Values : TArray<T>; From, Through : Integer; OnValue : TValueHandler<T>; Wait : boolean); overload; inline;
    class procedure Each<T>(Values : TArray<T>; From, Through : Integer; OnValue : TValueHandler<T>; OnProgress : TEachProgress; Wait : boolean); overload; inline;

    class procedure Each<T>(Values : IEnumerable<T>; OnValue : TValueHandler<T>); overload; inline;
    class procedure Each<T>(Values : IEnumerable<T>; OnValue : TValueHandler<T>; Wait : boolean); overload; inline;

    class procedure ScheduleFor(MSFromNow : Cardinal; AJob : TProc); overload;
    class procedure ScheduleFor(When : TDateTime; AJob : TProc); overload;
    class procedure OnIntervalDo(MSInterval : Cardinal; AJob : TProc);

    class constructor Create;
  end;

var
  Jobs : IJobs;

implementation

uses
  System.Classes,
  System.DateUtils,
  System.Diagnostics,
  cocinasync.global.spinwait;

const
  MAX_SPIN_COUNT = 1000;
  MAX_SPIN_PAUSE = 5000;

type
  TJobs = class;

  TJobRunner = class(TThread)
  strict private
    class var FPauseEvent : TEvent;
  strict private
    [Weak]
    FJobs : TJobs;
    FName : string;
  protected
    procedure Execute; override;
  public
    constructor Create(Jobs : TJobs; JobNumber : integer); reintroduce; virtual;
    class constructor Create;
    class destructor Destroy;
    class property PauseEvent : TEvent read FPauseEvent;
  end;

  TJobs = class(TInterfacedObject, IJobs)
  strict private
    FTerminating : boolean;
    FRunners : TQueue<TJobRunner>;
    FJobs : TQueue<IJob>;
    FNextJobs : TStack<IJob>;
    FName : string;
    procedure TerminateRunners;
    function RolloverNumber : integer; inline;
  private
    FJobRunnerCount : integer;
    FJobsInProcess : integer;
  public
    constructor Create(RunnerCount : Integer; MaxJobs : Integer = 4096; const AName : string = ''); reintroduce; virtual;
    destructor Destroy; override;

    function Next : IJob; inline;
    function Name : string;
    procedure Call(const DoIt : TProc); overload; inline;
    procedure Call(const Job : IJob); overload; inline;
    function Queue(const DoIt : TProc) : IJob; overload; inline;
    function Queue(const Job : IJob) : IJob; overload; inline;
    procedure WaitForAll(Timeout : Cardinal = INFINITE); inline;
    property Terminating : boolean read FTerminating;
    function Count : Integer;
    function RunnerCount : Integer;
  end;

var
  THREADS_TERMINATING : boolean;

{ TJobManager }

class procedure TJobManager.Call(const AJob: TProc; AJobs: IJobs);
var
  j : IJobs;
begin
  if Assigned(AJobs) then
    j := AJobs
  else
    j := Jobs;
  j.Call(AJob);
end;

class procedure TJobManager.Call<T>(const WithValue: T; const AJob: TValueHandler<T>; AJobs : IJobs = nil);
var
  j : IJobs;
begin
  if Assigned(AJobs) then
    j := AJobs
  else
    j := Jobs;

  j.Call(
    procedure
    begin
      AJob(WithValue);
    end
  );
end;

class constructor TJobManager.Create;
begin
  FEventClass := TEvent;
end;

class function TJobManager.CreateJobs(RunnerCount : Cardinal = 0; MaxJobs : Integer = 4096; const Name : string = '') : IJobs;
var
  iCnt : Cardinal;
begin
  if RunnerCount = 0 then
    iCnt := CPUCount*4  // default to 4 threads per native/hyperthreaded processing unit.
  else
    iCnt := RunnerCount;

  if iCnt < 6 then
    iCnt := 6;

  Result := TJobs.Create(iCnt, MaxJobs, Name);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; OnValue: TValueHandler<T>);
begin
  Each<T>(Values, 0, Length(Values)-1, OnValue, nil, False);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; OnValue: TValueHandler<T>; OnProgress: TEachProgress);
begin
  Each<T>(Values, 0, Length(Values)-1, OnValue, OnProgress, False);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; OnValue: TValueHandler<T>; Wait: boolean);
begin
  Each<T>(Values, 0, Length(Values)-1, OnValue, nil, Wait);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; OnValue: TValueHandler<T>; OnProgress: TEachProgress; Wait: boolean);
begin
  Each<T>(Values, 0, Length(Values)-1, OnValue, OnProgress, Wait);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; From, Through: Integer; OnValue: TValueHandler<T>);
begin
  Each<T>(Values, From, Through, OnValue, nil, False);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; From, Through: Integer; OnValue: TValueHandler<T>; Wait: boolean);
begin
  Each<T>(Values, From, Through, OnValue, nil, Wait);
end;

class procedure TJobManager.Each<T>(Values: TArray<T>; From, Through: Integer; OnValue: TValueHandler<T>; OnProgress: TEachProgress);
begin
  Each<T>(Values, From, Through, OnValue, OnProgress, False);
end;

class procedure TJobManager.Each<T>(Values : TArray<T>; From, Through : Integer; OnValue : TValueHandler<T>; OnProgress : TEachProgress; Wait : boolean);
var
  iFinished : integer;
  iCnt : Integer;
  i: Integer;
  jobs : TJobQueue;
begin
  if From > Through then
    exit;
  iCnt := Through - From;
  iFinished := 0;

  if Wait then
    jobs := TJobQueue.Create(iCnt+2)
  else
    jobs := nil;

  if Assigned(OnProgress) and Wait then
    for i := From to Through do
      jobs.Enqueue(LaunchJobForEach<T>(Values[i],
        procedure(const Value : T)
        begin
          OnValue(Value);
          OnProgress(TInterlocked.Increment(iFinished), iCnt)
        end
      ))
  else if Assigned(OnProgress) then
    for i := From to Through do
      TJobManager.Call<T>(Values[i],
        procedure(const Value : T)
        begin
          OnValue(Value);
          OnProgress(TInterlocked.Increment(iFinished), iCnt)
        end
      )
  else if Wait then
    for i := From to Through do
      jobs.Enqueue(LaunchJobForEach<T>(Values[i], OnValue))
  else
    for i := From to Through do
      TJobManager.Call<T>(Values[i], OnValue);

  if Wait then
  begin
    jobs.WaitForAll;
    jobs.Free;
  end;
end;

class procedure TJobManager.Each<T>(Values : IEnumerable<T>; OnValue : TValueHandler<T>);
begin
  Each<T>(Values, OnValue, False);
end;

class procedure TJobManager.Each(From, Through: Integer; OnValue: TValueHandler<Integer>);
begin
  Each(From, Through, 1, OnValue);
end;

class procedure TJobManager.Each(From, Through: Int64; OnValue: TValueHandler<Int64>);
begin
  Each(From, Through, 1, OnValue);
end;

class procedure TJobManager.Each(From, Through: Cardinal; OnValue: TValueHandler<Cardinal>);
begin
  Each(From, Through, 1, OnValue);
end;

class procedure TJobManager.Each(From, Through: UInt64; OnValue: TValueHandler<UInt64>);
begin
  Each(From, Through, 1, OnValue);
end;

class procedure TJobManager.Each(From, Through, Stride: Integer; OnValue: TValueHandler<Integer>);
var
  i : integer;
begin
  i := From;
  while i <= Through do
  begin
    Call<Integer>(i, OnValue);
    inc(i, Stride);
  end;
end;

class procedure TJobManager.Each(From, Through, Stride: Int64; OnValue: TValueHandler<Int64>);
var
  i : Int64;
begin
  i := From;
  while i <= Through do
  begin
    Call<Int64>(i, OnValue);
    inc(i, Stride);
  end;
end;

class procedure TJobManager.Each(From, Through, Stride: Cardinal; OnValue: TValueHandler<Cardinal>);
var
  i : Cardinal;
begin
  i := From;
  while i <= Through do
  begin
    Call<Cardinal>(i, OnValue);
    inc(i, Stride);
  end;
end;

class procedure TJobManager.Each(From, Through, Stride: UInt64; OnValue: TValueHandler<UInt64>);
var
  i : UInt64;
begin
  i := From;
  while i <= Through do
  begin
    Call<UInt64>(i, OnValue);
    inc(i, Stride);
  end;
end;

class procedure TJobManager.Each<T>(Values : IEnumerable<T>; OnValue : TValueHandler<T>; Wait : boolean);
var
  jobs : TJobQueue;
  enum : IEnumerator<T>;
begin
  enum := Values.GetEnumerator;
  if Wait then
  begin
    jobs := TJobQueue.Create(4096);
    try
      enum.Reset;
      repeat
        jobs.Enqueue(LaunchJobForEach<T>(enum.Current, OnValue));
      until (not enum.MoveNext);

      jobs.WaitForAll;
    finally
      jobs.Free;
    end;
  end else
  begin
    enum.Reset;
    repeat
      Call<T>(enum.Current, OnValue);
    until (not enum.MoveNext);
  end;
end;

class function TJobManager.Execute(const AJob: TProc; AJobs : IJobs = nil; const AName : string = ''): IJob;
begin
  Result := Job(AJob);
  if AJobs = nil then
    AJobs := Jobs;
  AJobs.Queue(Result);
end;

class function TJobManager.Execute<T>(const AJob: TFunc<T>; AJobs : IJobs = nil; const AName : string = ''): IJob<T>;
begin
  Result := Job<T>(AJob);
  if AJobs = nil then
    AJobs := Jobs;
  AJobs.Queue(Result);
end;

class function TJobManager.Execute(const AJob: TProc; AQueue: TJobQueue; AJobs : IJobs = nil; const AName : string = ''): IJob;
begin
  Result := Job(AJob);
  AQueue.Enqueue(Result);
  if AJobs = nil then
    AJobs := Jobs;
  AJobs.Queue(Result);
end;

class function TJobManager.Execute<T>(const AJob: TFunc<T>; AQueue: TJobQueue<T>; AJobs : IJobs = nil; const AName : string = ''): IJob<T>;
begin
  Result := Job<T>(AJob);
  AQueue.Enqueue(Result);
  if AJobs = nil then
    AJobs := Jobs;
  AJobs.Queue(Result);
end;

class procedure TJobManager.HideMonitor;
begin
  if Assigned(FMonitor) then
    FMonitor.OnHideMonitor();
end;

class function TJobManager.Job(const AJob: TProc; const AName : string = ''; DefaultDone : boolean = false): IJob;
begin
  Result := TDefaultJob<Boolean>.Pool.Pop;
  if not Assigned(Result) then
    Result := TDefaultJob<Boolean>.Create(AJob,nil, AName, DefaultDone)
  else
    (Result as TDefaultJob<Boolean>).Reset(AName, AJob, nil, DefaultDone);
end;

class function TJobManager.Job<T>(const AJob: TFunc<T>; const AName : string = ''): IJob<T>;
begin
  Result := TDefaultJob<T>.Pool.Pop;
  if not Assigned(Result) then
    Result := TDefaultJob<T>.Create(nil, AJob, AName, False)
  else
    (Result as TDefaultJob<T>).Reset(AName, nil, AJob, False);
end;

class function TJobManager.LaunchJobForEach<T>(const Value: T; OnValue : TValueHandler<T>) : IJob;
begin
  Result := TJobManager.Execute(
    procedure
    begin
      OnValue(Value);
    end
  );
end;

class procedure TJobManager.OnIntervalDo(MSInterval: Cardinal; AJob: TProc);
var
  DoProc : TProc;
begin
  DoProc :=
    procedure
    begin
      AJob();

      TJobManager.ScheduleFor(MSInterval, DoProc);
    end;

  DoProc();
end;

class procedure TJobManager.ProcessQueue<T>(AQueue: TQueue<T>; AJob: TValueHandler<T>;
  WaitForItems : boolean = false; AOnWait: TProc<boolean> = nil);
var
  i: Integer;
  jobs : TJobQueue;
  bAbort : Boolean;
  sw : TSpinWait;
begin
  bAbort := False;
  repeat
    jobs := TJobQueue.Create(AQueue.Size);
    try
      for i := AQueue.Count-1 downto 0 do
        jobs.Enqueue(
          TJobManager.Execute(
            procedure
            var
              val : T;
            begin
              if bAbort then
                exit;
              val := AQueue.Dequeue;
              if Assigned(AJob) then
                AJob(val);
            end
          )
        );

      if Assigned(AOnWait) then
      begin
        while not jobs.WaitForAll(10) do
          AOnWait(bAbort);
      end else
        jobs.WaitForAll;
    finally
      jobs.Free;
    end;

    if WaitForItems then
    begin
      sw.Reset;
      while AQueue.Count = 0 do
      begin
        if sw.NextSpinCycleWillYield and Assigned(AOnWait) then
          AOnWait(bAbort);
        sw.SpinCycle;
      end;
    end;
  until (not WaitForItems) or bAbort;
end;

class procedure TJobManager.RegisterMonitor(Monitor: IJobMonitor);
begin
  FMonitor := Monitor;
end;

class procedure TJobManager.ScheduleFor(MSFromNow: Cardinal; AJob: TProc);
begin
  TJobManager.ScheduleFor(IncMillisecond(Now, MSFromNow), AJob);
end;

class procedure TJobManager.ScheduleFor(When: TDateTime; AJob: TProc);
begin
  TThread.CreateAnonymousThread(
    procedure
    begin
      while not THREADS_TERMINATING do
      begin
        if Now < When then
          AJob();
        Sleep(100);
      end;
    end
  ).Start;
end;

class procedure TJobManager.ShowMonitor;
begin
  if Assigned(FMonitor) then
    FMonitor.OnShowMonitor;
end;

class procedure TJobManager.UnregisterMonitor(Monitor: IJobMonitor);
begin
  TInterlocked.CompareExchange(Pointer(FMonitor), nil, Pointer(Monitor));
end;

{ TJobs }

procedure TJobs.Call(const DoIt: TProc);
begin
  Call(TJobManager.Job(DoIt, '',True));
end;

procedure TJobs.Call(const Job: IJob);
begin
  if FTerminating then
    raise Exception.Create('Cannot queue while Jobs are terminating.');

  if FJobs.Count > RolloverNumber then
  begin
    FNextJobs.Push(Job);
  end else
    FJobs.Enqueue(Job);
  TJobRunner.PauseEvent.SetEvent;
end;

function TJobs.Count: Integer;
begin
  Result := FJobs.Count;
end;

constructor TJobs.Create(RunnerCount: Integer; MaxJobs : Integer = 4096; const AName : string = '');
begin
  inherited Create;
  if AName = '' then
    FName := Classname+'($'+IntToHex(Integer(@Self),SizeOf(Pointer))+')'
  else
    FName := AName;
  FTerminating := False;
  FJobs := TQueue<IJob>.Create(MaxJobs);
  FJobRunnerCount := 0;
  FJobsInProcess := 0;
  FNextJobs := TStack<IJob>.Create;
  FRunners := TQueue<TJobRunner>.Create(RunnerCount+1);
  while FRunners.Count < RunnerCount do
    FRunners.Enqueue(TJobRunner.Create(Self,FRunners.Count+1));
end;

destructor TJobs.Destroy;
begin
  TJobRunner.PauseEvent.SetEvent;
  TerminateRunners;
  FJobs.Free;
  FNextJobs.Free;
  FRunners.Free;
  inherited;
end;

function TJobs.Name: string;
begin
  Result := FName;
end;

function TJobs.Next: IJob;
var
  j : IJob;
begin
  Result := FJobs.Dequeue;
  if (Result = nil) then
  begin
    repeat
      j := FNextJobs.Pop;
      if Assigned(j) then
        FJobs.Enqueue(j);
    until (FJobs.Count >= RolloverNumber) or not Assigned(j);
  end;
end;

function TJobs.Queue(const DoIt: TProc) : IJob;
begin
  Result := Queue(TJobManager.Job(DoIt));
end;

function TJobs.Queue(const Job : IJob) : IJob;
begin
  if FTerminating then
    raise Exception.Create('Cannot queue while Jobs are terminating.');
  Result := Job;
  if FJobs.Count > RolloverNumber then
  begin
    FNextJobs.Push(Job);
  end else
    FJobs.Enqueue(Job);
  TJobRunner.PauseEvent.SetEvent;
end;

function TJobs.RolloverNumber: integer;
begin
  Result := Round(FJobs.Size * 0.80);
end;

function TJobs.RunnerCount: Integer;
begin
  Result := FRunners.Count;
end;

procedure TJobs.TerminateRunners;
var
  r : TJobRunner;
  rq : System.Generics.Collections.TQueue<TJobRunner>;
begin
  FTerminating := True;
  WaitForAll(3000);
  FJobs.Clear;

  rq := System.Generics.Collections.TQueue<TJobRunner>.Create;
  try
    repeat
      r := FRunners.Dequeue;
      if not Assigned(r) then
        break;
      r.Terminate;
      rq.Enqueue(r);
    until not Assigned(r);

    while FJobRunnerCount > 0 do
      Sleep(10);

    while rq.Count > 0 do
    begin
      r := rq.Dequeue;
      r.Free;
    end;
  finally
    rq.Free;
  end;
end;

procedure TJobs.WaitForAll(Timeout : Cardinal = INFINITE);
var
  timer : TStopWatch;
  sw : TSpinWait;
begin
  timer := TStopWatch.StartNew;
  sw.Reset;
  while ((FJobs.Count > 0) or (FJobsInProcess > 0)) and
        (  (Timeout = 0) or
           ((Timeout > 0) and (timer.ElapsedMilliseconds <= Timeout))
        ) do
    sw.SpinCycle;
end;

{ TJobRunner }

constructor TJobRunner.Create(Jobs : TJobs; JobNumber : integer);
begin
  inherited Create(False);
  FJobs := Jobs;
  FName := FJobs.Name+'.'+JobNumber.ToString;
  FreeOnTerminate := False;
end;

class constructor TJobRunner.Create;
begin
  FPauseEvent := TEvent.Create;
end;

class destructor TJobRunner.Destroy;
begin
  FPauseEvent.Free;
end;

procedure TJobRunner.Execute;
var
  wait : TSpinWait;
  job : IJob;
begin
  TInterlocked.Increment(FJobs.FJobRunnerCount);
  try
    wait.Reset;
    while not Terminated do
    begin
      job := FJobs.Next;
      if job <> nil then
      begin
        if FJobs.Terminating then
          exit;

        TInterlocked.Increment(FJobs.FJobsInProcess);
        try
          if Assigned(TJobManager.FMonitor) then
            TJobManager.FMonitor.OnBeginJob(FName, job.Name);
          try
            wait.Reset;
            job.SetupJob;
            try
              job.ExecuteJob;
            finally
              job.FinishJob;
            end;
          finally
            if Assigned(TJobManager.FMonitor) then
              TJobManager.FMonitor.OnEndJob(FName, job.Name);
          end;
        finally
          TInterlocked.Decrement(FJobs.FJobsInProcess);
        end;
      end else
        wait.SpinCycle;

      if wait.Count > MAX_SPIN_COUNT then
      begin
        FPauseEvent.WaitFor(MAX_SPIN_PAUSE);
        wait.Reset;
      end;
    end;
  finally
    TInterlocked.Decrement(FJobs.FJobRunnerCount);
  end;
end;

{ TDefaultJob }

constructor TDefaultJob<T>.Create(ProcToExecute : TProc; FuncToExecute : TFunc<T>; AName : string; DefaultDone : boolean);
begin
  inherited Create;
  TInterlocked.Increment(CreateCount);
  FEvent := TJobManager.EventClass.Create;
  Reset(AName, ProcToExecute, FuncToExecute, DefaultDone);
end;

class constructor TDefaultJob<T>.Create;
begin
  FPool := TStack<IJob<T>>.Create;
end;

destructor TDefaultJob<T>.Destroy;
begin
  FEvent.Free;
  inherited;
end;

procedure TDefaultJob<T>.Done;
begin
  FDone := True;
end;

class destructor TDefaultJob<T>.Destroy;
begin
  FPool.Free;
end;

procedure TDefaultJob<T>.ExecuteJob;
var
  sw : TStopWatch;
begin
  if not FException.Triggered then
    try
      sw := TStopWatch.Create;
      try
        sw.Start;
        if Assigned(FProcToExecute) then
          FProcToExecute()
        else if Assigned(FFuncToExecute) then
          FResult := FFuncToExecute();
      finally
        FExecutionTime := sw.ElapsedMilliseconds;
      end;
      if FDone then
        FPool.Push(Self);
    except
      on e: Exception do
      begin
        FException.Update(e);
      end;
    end;
  SetEvent;
end;

function TDefaultJob<T>.ExecutionTime: Cardinal;
begin
  Result := FExecutionTime;
end;

procedure TDefaultJob<T>.FinishJob;
begin
  // Nothing to finish
end;

function TDefaultJob<T>.Name: string;
begin
  Result := FName;
end;

procedure TDefaultJob<T>.Reset(AName: string; ProcToExecute: TProc; FuncToExecute: TFunc<T>; DefaultDone : boolean);
begin
  TInterlocked.Increment(UseCount);
  FDone := DefaultDone;
  FExecutionTime := 0;
  if AName = '' then
    FName := ClassName + '($' + IntToHex(Integer(@Self), SizeOf(Pointer)) + ')'
  else
    FName := AName;
  FException := TJobException.Init;
  FResult := T(nil);
  FProcToExecute := ProcToExecute;
  FFuncToExecute := FuncToExecute;
  FEvent.ResetEvent;
end;

procedure TDefaultJob<T>.RaiseExceptionIfExists;
begin
  FException.RaiseExceptionIfExists(FName);
end;

function TDefaultJob<T>.Result: T;
begin
  Wait;
  RaiseExceptionIfExists;
  Result := FResult;
end;

procedure TDefaultJob<T>.SetEvent;
begin
  FEvent.SetEvent;
end;

procedure TDefaultJob<T>.SetupJob;
begin
  // Nothing to Setup
end;

procedure TDefaultJob<T>.Wait(var Completed: boolean; Timeout: Cardinal = INFINITE);
var
  wr : TWaitResult;
begin
  wr := FEvent.WaitFor(Timeout);
  Completed := wr <> TWaitResult.wrTimeout;
  RaiseExceptionIfExists;
end;

function TDefaultJob<T>.Wait(Timeout: Cardinal = INFINITE): boolean;
var
  wr : TWaitResult;
begin
  wr := FEvent.WaitFor(Timeout);
  Result := wr <> TWaitResult.wrTimeout;
  RaiseExceptionIfExists;
end;


{ TJobQueue }

procedure TJobQueue.Abort;
var
  j : IJob;
begin
  repeat
    j := Dequeue;
  until j = nil;
end;

function TJobQueue.WaitForAll(Timeout: Cardinal = INFINITE): boolean;
var
  j : IJob;
  timer : TStopWatch;
begin
  timer := TStopWatch.StartNew;
  Result := True;
  while Count > 0 do
  begin
    j := Dequeue;
    if not j.Wait(1) then
      Enqueue(j);
    if (Count > 0) and (timer.ElapsedMilliseconds >= Timeout) then
    begin
      Result := False;
      break;
    end;
  end;
end;

{ TJobQueue<T> }

procedure TJobQueue<T>.Abort;
var
  j : IJob;
begin
  repeat
    j := Dequeue;
  until j = nil;
end;

function TJobQueue<T>.WaitForAll(Timeout: Cardinal = INFINITE): boolean;
var
  j : IJob<T>;
  timer : TStopWatch;
begin
  timer := TStopWatch.StartNew;
  Result := True;
  while Count > 0 do
  begin
    j := Dequeue;
    if not j.Wait(1) then
      Enqueue(j);
    if (Count > 0) and (timer.ElapsedMilliseconds >= Timeout) then
    begin
      Result := False;
      break;
    end;
  end;
end;

{ TJobException }

class function TJobException.Init: TJobException;
begin
  Result.Clss := '';
  Result.Msg := '';
  Result.Addr := nil;
  Result.CallStack := '';
  Result.Triggered := False;
end;

procedure TJobException.RaiseExceptionIfExists(const JobName : string);
begin
  if Self.Triggered then
    raise EJobExecutionFailure.Create('Job "'+ JobName +'" Exception raised "'+Self.Clss+': '+Self.Msg+'" Stack: '+Self.CallStack) at Self.Addr;
end;


procedure TJobException.Update(E: Exception);
begin
  if Assigned(E) then
  begin
    Self.Clss := E.ClassName;
    Self.Msg := E.Message;
    Self.Addr := ExceptAddr;
    Self.CallStack := E.StackTrace;
    Self.Triggered := True;
  end;
end;

initialization
  Jobs := TJobManager.CreateJobs(0,4096,'Default');
  CreateCount := 0;
  UseCount := 0;
  THREADS_TERMINATING := False;

finalization
  THREADS_TERMINATING := True;
  Jobs.WaitForAll;

end.
