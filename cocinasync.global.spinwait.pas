unit cocinasync.global.spinwait;

interface

uses System.SysUtils, System.Classes, System.TimeSpan;

type
  TSpinWait = record
  private const
    YieldThreshold = 10;
    Sleep10Threshold = 80;
    Sleep1Threshold = 40;
    Sleep0Threshold = 20;
  private
    FCount: Integer;
    function GetNextSpinCycleWillYield: Boolean;
  public
    procedure Reset;
    procedure SpinCycle;

    class procedure SpinUntil(const ACondition: TFunc<Boolean>); overload; static;
    class function SpinUntil(const ACondition: TFunc<Boolean>; Timeout: LongWord): Boolean; overload; static;
    class function SpinUntil(const ACondition: TFunc<Boolean>; const Timeout: TTimeSpan): Boolean; overload; static;

    property Count: Integer read FCount;
    property NextSpinCycleWillYield: Boolean read GetNextSpinCycleWillYield;
  end;


implementation

uses System.Diagnostics, System.RTLConsts;

{ TSpinWait }

function TSpinWait.GetNextSpinCycleWillYield: Boolean;
begin
  Result := (FCount > YieldThreshold) or (CPUCount = 1);
end;

procedure TSpinWait.Reset;
begin
  FCount := 0;
end;

procedure TSpinWait.SpinCycle;
var
  SpinCount: Integer;
begin
  if NextSpinCycleWillYield then
  begin
    if FCount >= YieldThreshold then
      SpinCount := FCount - YieldThreshold
    else
      SpinCount := FCount;
    if SpinCount mod Sleep10Threshold = Sleep10Threshold - 1 then
      TThread.Sleep(10)
    else if SpinCount mod Sleep1Threshold = Sleep1Threshold - 1 then
      TThread.Sleep(1)
    else if SpinCount mod Sleep0Threshold = Sleep0Threshold - 1 then
      TThread.Sleep(0)
    else
      TThread.Yield;
  end else
    TThread.SpinWait(4 shl FCount);
  Inc(FCount);
  if FCount < 0 then
    FCount := YieldThreshold + 1;
end;

class procedure TSpinWait.SpinUntil(const ACondition: TFunc<Boolean>);
begin
  SpinUntil(ACondition, INFINITE);
end;

class function TSpinWait.SpinUntil(const ACondition: TFunc<Boolean>; Timeout: LongWord): Boolean;
var
  Timer: TStopwatch;
  Wait: TSpinWait;
begin
  if @ACondition = nil then
    raise EArgumentNilException.CreateRes(@SArgumentNil);
  Timer := TStopwatch.StartNew;
  Wait.Reset;
  while not ACondition() do
  begin
    if Timeout = 0 then
      Exit(False);
    Wait.SpinCycle;
    if (Timeout <> INFINITE) and Wait.NextSpinCycleWillYield and (Timeout <= Timer.ElapsedMilliseconds) then
      Exit(False);
  end;
  Result := True;
end;

class function TSpinWait.SpinUntil(const ACondition: TFunc<Boolean>; const Timeout: TTimeSpan): Boolean;
var
  Total: Int64;
begin
  Total := Trunc(Timeout.TotalMilliseconds);
  if (Total < 0) or (Total > $7FFFFFFF) then
    raise EArgumentOutOfRangeException.CreateResFmt(@sInvalidTimeoutValue, [string(Timeout)]);
  Result := SpinUntil(ACondition, LongWord(Total));
end;



end.
