unit cocinasync.flux.dispatcher;

interface

uses
  System.SysUtils,
  System.Classes,
  System.SyncObjs,
  cocinasync.flux.action,
  System.Generics.Collections,
  cocinasync.collections,
  cocinasync.flux.store;

type
  TFluxErrorHandler = reference to procedure(Store : TObject; Action : TBaseAction; E : Exception);

  TFluxDispatcher = class(TObject)
  strict private
    type
      TStoreRegistration<AC : TBaseAction> = class(TObject)
        Store : TObject;
        ClassType : TClass;
        Handler : TActionHandler<AC>;
        type
          PAC = ^AC;
        constructor Create(AStore : TObject; AHandler : TActionHandler<AC>);
        function AsBase : TStoreRegistration<TBaseAction>;
      end;

      TActionRegistry = TList<TStoreRegistration<TBaseAction>>;
      TSessionIndex = TDictionary<string, TActionRegistry>;
      TNotificationIndex = TDictionary<TActionClass, TSessionIndex>;

      TDispatchThread = class(TThread)
      strict private
        FCS : TMREWSync;
        FErrorHandler : TFluxErrorHandler;
        FActionEvent : TEvent;
        FDispatchQueue : TQueue<TBaseAction>;
        FNotifications : TNotificationIndex;
        FDispatchDepth : Integer;
        FLastAction : TPair<TDateTime, TClass>;
      protected
        procedure Execute; override;
      public
        constructor Create(Notifications : TNotificationIndex);
        destructor Destroy; override;

        procedure EnQueue(Action : TBaseAction);
        function Dequeue : TBaseAction;
        procedure WaitOnAction;
        procedure BeginNotifyRead;
        function BeginNotifyWrite : boolean;
        procedure EndNotifyRead;
        procedure EndNotifyWrite;
        property ErrorHandler : TFluxErrorHandler read FErrorHandler write FErrorHandler;
        property DispatchDepth : Integer read FDispatchDepth;
      end;
  private
    FDispatchThread : TDispatchThread;
    FNotifications : TNotificationIndex;
  private
    procedure EnsureActionExistsForNotifications(Action: TActionClass; Sessions : TArray<String>);
    function GetErrorHandler: TFluxErrorHandler;
    procedure SetErrorhandler(const Value: TFluxErrorHandler);
  public
    procedure Register<AC : TBaseAction>(Store : TObject; Handler : TActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TObject; Handler : TConfigurableActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TObject; Event : TActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TObject; Event : TConfigurableActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Handler : TActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Handler : TConfigurableActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Event : TActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Event : TConfigurableActionEvent<AC>); overload;

    procedure Register<AC : TBaseAction>(Session : string; Store : TObject; Handler : TActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Session : string; Store : TObject; Handler : TConfigurableActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Sessions : TArray<string>; Store : TObject; Handler: TActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Sessions: TArray<string>; Store: TObject; Handler: TConfigurableActionHandler<AC>); overload;
    procedure Register<AC : TBaseAction>(Session : string; Store : TObject; Event : TActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Session : string; Store : TObject; Event : TConfigurableActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Sessions : TArray<string>; Store : TObject; Event : TActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Sessions : TArray<string>; Store : TObject; Event : TConfigurableActionEvent<AC>); overload;

    procedure Unregister<AC : TBaseAction>(Store : TObject); overload;
    procedure Unregister<AC : TBaseAction>(Store : TBaseDataModuleStore); overload;
    procedure WaitFor(StoreIDs : TArray<TStoreID>; ThenDo : TProc);
    procedure Dispatch(Action : TBaseAction); reintroduce;
    function IsDispatching : Boolean;

    property ErrorHandler : TFluxErrorHandler read GetErrorHandler write SetErrorhandler;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  System.DateUtils;

{ TFluxDispatcher.TStoreRegistration }

function TFluxDispatcher.TStoreRegistration<AC>.AsBase: TStoreRegistration<TBaseAction>;
begin
  Result := TStoreRegistration<TBaseAction>(Self);
end;

constructor TFluxDispatcher.TStoreRegistration<AC>.Create(AStore : TObject; AHandler : TActionHandler<AC>);
begin
  inherited Create;
  Store := AStore;
  ClassType := AC;
  Handler := AHandler;
end;

{ TFluxDispatcher }

constructor TFluxDispatcher.Create;
begin
  inherited Create;
  FNotifications := TNotificationIndex.Create;
  FDispatchThread := TDispatchThread.Create(FNotifications);
end;

destructor TFluxDispatcher.Destroy;
var
  arySession : TArray<TPair<string, TActionRegistry>>;
  ary : TArray<TPair<TActionClass, TSessionIndex>>;
  p : TPair<TActionClass, TSessionIndex>;
  r : TPair<string, TActionRegistry>;
begin
  TStores.FreeAllStores;
  while not FDispatchThread.BeginNotifyWrite do
    Sleep(1);
  try
    ary := FNotifications.ToArray;
    for p in ary do
    begin
      arySession := p.Value.ToArray;
      for r in arySession do
      begin
        while r.Value.Count > 0 do
        begin
          {$IF CompilerVersion > 30}
          r.Value.ExtractAt(0).Free;
          {$ELSE}
          r.Value.Extract(r.Value.Items[0]).Free;
          {$ENDIF}
        end;
        r.Value.Free;
      end;

      p.Value.Free;
    end;
  finally
    FDispatchThread.EndNotifyWrite;
  end;
  FNotifications.Free;
  FDispatchThread.Terminate;
  Sleep(100);
  FDispatchThread.Free;
  inherited;
end;

procedure TFluxDispatcher.EnsureActionExistsForNotifications(Action: TActionClass; Sessions : TArray<String>);
var
  s : string;
begin
  FDispatchThread.BeginNotifyRead;
  try
    if not FNotifications.ContainsKey(Action) then
    begin
      while not FDispatchThread.BeginNotifyWrite do
        Sleep(1);

      try
        FNotifications.Add(Action, TSessionIndex.Create);
      finally
        FDispatchThread.EndNotifyWrite;
      end;
      for s in Sessions do
        if not FNotifications[Action].ContainsKey(s) then
        begin
          FNotifications[Action].Add(s, TActionRegistry.Create);
        end;
    end;
  finally
    FDispatchThread.EndNotifyRead;
  end;
end;

function TFluxDispatcher.GetErrorHandler: TFluxErrorHandler;
begin
  Result := FDispatchThread.ErrorHandler;
end;

procedure TFluxDispatcher.Dispatch(Action: TBaseAction);
begin
  FDispatchThread.Enqueue(Action);
end;

function TFluxDispatcher.IsDispatching: Boolean;
begin
  Result := FDispatchThread.DispatchDepth > 0;
end;

procedure TFluxDispatcher.Register<AC>(Session: string; Store: TObject;
  Handler: TActionHandler<AC>);
begin
  Register<AC>([Session],Store,Handler);
end;

procedure TFluxDispatcher.Register<AC>(Session: string; Store: TObject;
  Event: TActionEvent<AC>);
begin
  Register<AC>([Session],Store,Event);
end;

procedure TFluxDispatcher.Register<AC>(Sessions: TArray<string>; Store: TObject;
  Event: TActionEvent<AC>);
begin
  Register<AC>(Sessions, Store,
    procedure(Action : AC)
    begin
      Event(Store, Action);
    end
  );
end;

procedure TFluxDispatcher.Register<AC>(Store: TObject; Handler: TActionHandler<AC>);
begin
  Register<AC>([''], Store, Handler);
end;

procedure TFluxDispatcher.Register<AC>(Store: TObject; Event: TActionEvent<AC>);
begin
  Register<AC>([''], Store, Event);
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore; Event: TActionEvent<AC>);
begin
  Register<AC>(Store.BaseStore, Event);
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore; Handler: TActionHandler<AC>);
begin
  Register<AC>(Store.BaseStore, Handler);
end;

procedure TFluxDispatcher.Register<AC>(Sessions: TArray<string>; Store: TObject; Handler: TActionHandler<AC>);
begin
  Register<AC>(Sessions, Store,
    procedure(Action : AC; var UpdateTiming : TViewUpdateTiming)
    begin
      Handler(Action);
    end
  );
end;

procedure TFluxDispatcher.Register<AC>(Sessions: TArray<string>; Store: TObject; Handler: TConfigurableActionHandler<AC>);
var
  si : TSessionIndex;
  s: string;
begin
  FDispatchThread.BeginNotifyRead;
  try
    EnsureActionExistsForNotifications(AC, Sessions+['*']);
    si := FNotifications[AC];
    TMonitor.Enter(si);
    try
      for s in Sessions do
      begin
        if not si.ContainsKey(s) then
          si.Add(s, TActionRegistry.Create);

        si[s].Add(TStoreRegistration<AC>.Create(Store,
          procedure(Action : AC)
          var
            UpdateTiming : TViewUpdateTiming;
          begin
            try
              if (Store is TBaseStore) and (TBaseStore(Store).AutoViewUpdate) then
                if (TBaseStore(Store).MSToAccumulate > 0) then
                  UpdateTiming := TViewUpdateTiming.After
                else
                  UpdateTiming := TViewUpdateTiming.Now
              else
                UpdateTiming := TViewUpdateTiming.None;

              Handler(Action, UpdateTiming);

              if (Store is TBaseStore) then
                case UpdateTiming of
                  TViewUpdateTiming.None:  ; // Do Nothing
                  TViewUpdateTiming.Now:   TBaseStore(Store).UpdateViews;
                  TViewUpdateTiming.After: TBaseStore(Store).AccumulateUpdates;
                end;
            except
              raise;
            end;
          end
        ).AsBase);
      end;
    finally
      TMonitor.Exit(si);
    end;
  finally
    FDispatchThread.EndNotifyRead;
  end;
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore;
  Handler: TConfigurableActionHandler<AC>);
begin
  Register<AC>([''], Store.BaseStore, Handler);
end;

procedure TFluxDispatcher.Register<AC>(Store: TObject;
  Event: TConfigurableActionEvent<AC>);
begin
  Register<AC>([''], Store, Event);
end;

procedure TFluxDispatcher.Register<AC>(Store: TObject;
  Handler: TConfigurableActionHandler<AC>);
begin
  Register<AC>([''], Store, Handler);
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore;
  Event: TConfigurableActionEvent<AC>);
begin
  Register<AC>([''], Store.BaseStore, Event);
end;

procedure TFluxDispatcher.Register<AC>(Sessions: TArray<string>; Store: TObject;
  Event: TConfigurableActionEvent<AC>);
begin
  Register<AC>(Sessions, Store,
    procedure(Action : AC; var UpdateTiming : TViewUpdateTiming)
    begin
      Event(Store, Action, UpdateTiming);
    end
  );
end;

procedure TFluxDispatcher.Register<AC>(Session: string; Store: TObject;
  Event: TConfigurableActionEvent<AC>);
begin
  Register<AC>([Session], Store, Event);
end;

procedure TFluxDispatcher.Register<AC>(Session: string; Store: TObject;
  Handler: TConfigurableActionHandler<AC>);
begin
  Register<AC>([Session], Store, Handler);
end;

procedure TFluxDispatcher.SetErrorhandler(const Value: TFluxErrorHandler);
begin
  FDispatchThread.ErrorHandler := Value;
end;

procedure TFluxDispatcher.Unregister<AC>(Store: TObject);
  //    TActionRegistry = TList<TStoreRegistration<TBaseAction>>;
  //    TSessionIndex = TDictionary<string, TActionRegistry>;
  //    TNotificationIndex = TDictionary<TActionClass, TSessionIndex>;
var
  ary : TArray<TPair<TActionClass, TSessionIndex>>;
  arySessions : TArray<TPair<string, TActionRegistry>>;
  si : TPair<TActionClass, TSessionIndex>;
  ar : TPair<string, TActionRegistry>;
  s : string;
  item: TStoreRegistration<TBaseAction>;
begin
  FDispatchThread.BeginNotifyRead;
  try
    if not FNotifications.ContainsKey(AC) then
      exit;
    ary := FNotifications.ToArray;

    for si in ary do
    begin
      arySessions := si.Value.ToArray;

      for ar in arySessions do
      begin
        for item in ar.Value do
        begin
          if item.Store = Store then
          begin
            while not FDispatchThread.BeginNotifyWrite do
              sleep(1);
            try
              ar.Value.Remove(item);
              item.Free;
            finally
              FDispatchThread.EndNotifyWrite;
            end;
            //break;
          end;
        end;
      end;
    end;
  finally
    FDispatchThread.EndNotifyRead;
  end;
end;

procedure TFluxDispatcher.Unregister<AC>(Store: TBaseDataModuleStore);
begin
  Unregister<AC>(Store.BaseStore);
end;

procedure TFluxDispatcher.WaitFor(StoreIDs: TArray<TStoreID>; ThenDo: TProc);
begin
  // NOT IMPLEMENTED
end;


{ TFluxDispatcher.TDispatchThread }

procedure TFluxDispatcher.TDispatchThread.BeginNotifyRead;
begin
  FCS.BeginRead;
end;

function TFluxDispatcher.TDispatchThread.BeginNotifyWrite : boolean;
begin
  FCS.BeginWrite;
  Result := True;
end;

constructor TFluxDispatcher.TDispatchThread.Create(Notifications : TNotificationIndex);
begin
  inherited Create(False);
  FDispatchDepth := 0;
  FNotifications := Notifications;
  FDispatchQueue := TQueue<TBaseAction>.Create;
  FActionEvent := TEvent.Create;
  FCS := TMREWSync.Create;
end;

type
  TActionHacker = class(TBaseAction)

  end;

function TFluxDispatcher.TDispatchThread.Dequeue: TBaseAction;
begin
  // Grab next action to execute from queue.
  Result := FDispatchQueue.Dequeue;

  // if an action was found in the queue
  if Assigned(Result) then
  begin
    // if the action is incidental, we don't want to
    // repeat it if the last action was the same type.
    if (TActionHacker(Result).IsIncidental) then
    begin
      if (FLastAction.Value = Result.ClassType) and
         (MillisecondsBetween(FLastAction.Key, Now) < 250) then
      begin
        Result.Finish;
        exit(nil);
      end;
    end;
  end;
end;

destructor TFluxDispatcher.TDispatchThread.Destroy;
begin
  FCS.Free;
  FDispatchQueue.Free;
  FActionEvent.Free;
  inherited;
end;

procedure TFluxDispatcher.TDispatchThread.EndNotifyRead;
begin
  FCS.EndRead;
end;

procedure TFluxDispatcher.TDispatchThread.EndNotifyWrite;
begin
  FCS.EndWrite;
end;

procedure TFluxDispatcher.TDispatchThread.EnQueue(Action: TBaseAction);
begin
  if not Assigned(Action) then
    exit;

  FDispatchQueue.Enqueue(Action);
  FActionEvent.SetEvent;
end;

procedure TFluxDispatcher.TDispatchThread.Execute;
  procedure DispatchFor(Action : TBaseAction; Session : string);
  var
    si : TSessionIndex;
    lst : TList<TStoreRegistration<TBaseAction>>;
    item : TStoreRegistration<TBaseAction>;
  begin
    si := FNotifications[TActionClass(Action.ClassType)];
    if si.ContainsKey(Session) then
    begin
      lst := si[Session];

      TMonitor.Enter(lst);
      try
        for item in lst do
        begin
          if Assigned(item.Handler) then
          begin
            try
              item.Handler(Action)
            except
              on e: exception do
              begin
                if Assigned(FErrorHandler)  then
                begin
                  FErrorHandler(item, Action, E);
                end;
              end;
            end;
          end;
        end;
      finally
        TMonitor.Exit(lst);
      end;
    end;
  end;
var
  Action : TBaseAction;
begin
  while not Terminated do
  begin
    try
      Action := Dequeue;
      if Action = nil then
      begin
        WaitOnAction;
        Continue;
      end;
      FCS.BeginRead;
      try
        if FNotifications.ContainsKey(TActionClass(Action.ClassType)) then
        begin
          FLastAction := TPair<TDateTime, TClass>.Create(Now,Action.ClassType);
          TInterlocked.Increment(FDispatchDepth);
          try
            DispatchFor(Action, Action.Session);
            DispatchFor(Action, '*');
            Action.Finish;
          finally
            TInterlocked.Decrement(FDispatchDepth);
          end;
        end;
      finally
        FCS.EndRead;
      end;
    except
      on E: Exception do
      begin
        if Assigned(FErrorHandler) then
          FErrorHandler(nil, nil, E);
      end;
    end;
  end;
end;

procedure TFluxDispatcher.TDispatchThread.WaitOnAction;
begin
  FActionEvent.WaitFor(10);
  FActionEvent.ResetEvent;
end;

end.
