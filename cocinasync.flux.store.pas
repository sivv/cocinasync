unit cocinasync.flux.store;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  System.Rtti,
  System.SyncObjs,
  cocinasync.flux.action,
  cocinasync.async;

type
  {$SCOPEDENUMS ON}

  TBaseStore = class;
  TStoreID = string;

  TEventMethod<T> = procedure(Sender : TObject; Store : T) of object;

  EFluxBindException = class(Exception);

  TViewUpdateTiming = (None, Now, After);
  TActionEvent<AC : TBaseAction> = procedure(Sender : TObject; Action : AC) of object;
  TActionHandler<AC : TBaseAction> = reference to procedure(Action : AC);
  TConfigurableActionEvent<AC : TBaseAction> = procedure(Sender : TObject; Action : AC; var UpdateTiming : TViewUpdateTiming) of object;
  TConfigurableActionHandler<AC : TBaseAction> = reference to procedure(Action : AC; var UpdateTiming : TViewUpdateTiming);

  TBaseStore = class(TInterfacedObject)
  private type
    IStoreBinding = interface
      function Target: TComponent;
      function SourceProperty : string;
      function TargetProperty : string;
      function AsValue : TValue;
      procedure UpdateValue;
    end;
  public
    const INFINITE = High(Integer);
  private
    FAfterMSUpdate : Cardinal;
    FUpdateEvent : TEvent;
    FViews : TDictionary<TObject, TProc<TBaseStore>>;
    FBindings : TList<IStoreBinding>;
    FCS : TCriticalSection;
    FUpdateCount : UInt64;
    FSession : string;
    FDelayedUpdate : IDelayedDo;
    function DelayedUpdate : IDelayedDo;
  protected
    procedure SafeUpdate(OnUpdate : TProc); virtual;
    procedure Bind(const SourceProperty : string; Target : TComponent; const TargetProperty : string);
    procedure UnbindAll(Target : TComponent);
    procedure UpdateBindings;
    property  AfterMSUpdate : Cardinal read FAfterMSUpdate write FAfterMSUpdate;
    procedure DoUpdates; virtual;
  public
    constructor Create; reintroduce; virtual;
    constructor CreateWithSession(Session : string); virtual;
    destructor Destroy; override;
    function AutoViewUpdate : boolean; virtual;
    property UpdateCount : UInt64 read FUpdateCount;
    property Session : string read FSession;
    property MSToAccumulate : Cardinal read FAfterMSUpdate;

    {$If CompilerVersion > 30}
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Event : TEventMethod<SC>; FutureChangesOnly : boolean); overload;
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Handler : TProc<SC>; FutureChangesOnly : boolean); overload;
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Event : TEventMethod<SC>); overload;
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Handler : TProc<SC>); overload;
    function WaitForUpdate<SC : TBaseStore>(Handler : TProc<SC>; Timeout : Integer = INFINITE) : boolean; overload;
    {$ELSE}
    procedure RegisterForUpdates(Context : TObject; Event : TEventMethod<TBaseStore>; FutureChangesOnly : boolean); overload;
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseStore>; FutureChangesOnly : boolean); overload;
    procedure RegisterForUpdates(Context : TObject; Event : TEventMethod<TBaseStore>); overload;
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseStore>); overload;
    function WaitForUpdate(Handler : TProc<TBaseStore>; Timeout : Integer = INFINITE) : boolean; overload;
    {$IFEND}
    procedure UnregisterForUpdates(Context : TObject);
    procedure UpdateViews;
    procedure AccumulateUpdates;
  end;


  TBaseStoreClass = class of TBaseStore;

  TDataStoreHandler<SC : TBaseStore, constructor> = reference to procedure(Store : SC);
  TDataStoreValueHandler<SC : TBaseStore, constructor; T> = reference to function(Store : SC) : T;

  TStores = class sealed
  strict private
    type
      TSessionStores = TDictionary<TBaseStoreClass, TBaseStore>;
      TStoreIndex = TDictionary<String, TSessionStores>;
  strict private
    class var FStores : TStoreIndex;
    class var FCS : TCriticalSection;
  private
    class function Stores(Session : string) : TSessionStores;
  public
    class procedure FreeAllStores;
    class procedure WithData<SC : TBaseStore, constructor>(Session : string; OnData : TDataStoreHandler<SC>); overload;
    class procedure WithData<SC : TBaseStore, constructor>(OnData : TDataStoreHandler<SC>); overload;
    class function Value<SC : TBaseStore, constructor; T>(Session : string; OnData : TDataStoreValueHandler<SC,T>) : T; overload;
    class function Value<SC : TBaseStore, constructor; T>(OnData : TDataStoreValueHandler<SC,T>) : T; overload;
    class procedure WaitForUpdate<SC : TBaseStore, constructor>(Session : string; LastUpdateCount : UInt64; OnData : TDataStoreHandler<SC>; Timeout : Cardinal = TBaseStore.INFINITE);
    class function Data<SC : TBaseStore, constructor>(Session : string) : SC; overload;
    class function Data<SC : TBaseStore, constructor> : SC; overload;

    class procedure Init<SC : TBaseStore>(Session : string = '');

    // Bindings currently only support single level property names.  Items instead of Items.Text to assign a string list for example.
    // If you need more complex bindings, use the RegisterForUpdates overloads
    class procedure Bind<SC : TBaseStore, constructor>(Target : TComponent; const &PropertyName : String); overload;
    class procedure Bind<SC : TBaseStore, constructor>(const SourceProperty : string; Target : TComponent; const TargetProperty : String); overload;
    class procedure Bind<SC : TBaseStore, constructor>(Target : TComponent; const &PropertyName : String; const Session : string); overload;
    class procedure Bind<SC : TBaseStore, constructor>(const SourceProperty : string; Target : TComponent; const TargetProperty : String; const Session : string); overload;
    class procedure UnbindAll<SC : TBaseStore, constructor>(Target : TComponent); overload;
    class procedure UnbindAll<SC : TBaseStore, constructor>(Target : TComponent; const Session : string); overload;

    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Event : TEventMethod<SC>; FutureChangesOnly : boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Handler : TProc<SC>; FutureChangesOnly : boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Handler : TProc<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Event : TEventMethod<SC>; FutureChangesOnly : boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Handler : TProc<SC>; FutureChangesOnly : boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Handler : TProc<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Event : TEventMethod<SC>; FutureChangesOnly : Boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Handler : TProc<SC>; FutureChangesOnly : boolean); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Handler : TProc<SC>); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Sessions : TArray<string>; Context : TObject); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Session : string; Context : TObject); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Context : TObject); overload;
    class constructor Create;
    class destructor Destroy;
  end;

  TBaseDataModuleStore = class(TDataModule)
  private
    FBaseStore: TBaseStore;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    {$If CompilerVersion > 30}
    procedure RegisterForUpdates<SC : TBaseDataModuleStore>(Context : TObject; Handler : TProc<SC>; FutureChangesOnly : Boolean); overload;
    procedure RegisterForUpdates<SC : TBaseDataModuleStore>(Context : TObject; Handler : TProc<SC>); overload;
    {$ENDIF}
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseDataModuleStore>; FutureChangesOnly : Boolean); overload;
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseDataModuleStore>); overload;
    procedure UpdateViews; virtual;
    property BaseStore : TBaseStore read FBaseStore;
    procedure UnregisterForUpdates(Context : TObject);
  end;

implementation

type
  TBinding = class(TInterfacedObject, TBaseStore.IStoreBinding)
  private
    FOwner : TBaseStore;
    FTarget : TComponent;
    FSourceProperty : string;
    FTargetProperty : string;
    FSourceProp : TRTTIProperty;
    FTargetProp : TRTTIProperty;
    FIsInstanceProp: Boolean;
    FSourcePropInstance: TObject;
    FTargetPropInstance: TObject;
  public
    constructor Create(Owner : TBaseStore; Target: TComponent; const SourceProperty, TargetProperty : string);
    function Target : TComponent;
    function SourceProperty : string;
    function TargetProperty : string;
    function AsValue : TValue;
    procedure UpdateValue;
  end;

{ TBaseStore }

function TBaseStore.AutoViewUpdate: boolean;
begin
  Result := True;
end;

procedure TBaseStore.Bind(const SourceProperty: string; Target: TComponent; const TargetProperty : string);
begin
  FBindings.Add(
    TBinding.Create(Self, Target, SourceProperty, TargetProperty)
  );
end;

constructor TBaseStore.Create;
begin
  inherited;
  FBindings := TList<IStoreBinding>.Create;
  FUpdateEvent := TEvent.Create;
  FCS := TCriticalSection.Create;
  FViews := TDictionary<TObject, TProc<TBaseStore>>.Create;
end;

constructor TBaseStore.CreateWithSession(Session: string);
begin
  FSession := Session;
  Create;
end;

function TBaseStore.DelayedUpdate: IDelayedDo;
begin
  if not Assigned(FDelayedUpdate) then
    FDelayedUpdate := TAsync.AfterLastCall(FAfterMSUpdate,
      procedure
      begin
        UpdateViews;
      end
    );
  Result := FDelayedUpdate;
end;

destructor TBaseStore.Destroy;
begin
  FCS.Free;
  FViews.Free;
  FUpdateEvent.Free;
  FBindings.Free;
  inherited;
end;

procedure TBaseStore.DoUpdates;
var
  ary : TArray<TPair<TObject, TProc<TBaseStore>>>;
  a: TPair<TObject, TProc<TBaseStore>>;
begin
  UpdateBindings;

  TMonitor.Enter(FViews);
  try
    ary := FViews.ToArray;
  finally
    TMonitor.Exit(FViews);
  end;
  for a in ary do
  begin
    TAsync.SynchronizeIfInThread(
      procedure
      begin
        a.Value(Self);
      end
    );
  end;
  inc(FUpdateCount);
  FUpdateEvent.SetEvent;
  FUpdateEvent.ResetEvent;
end;

{$IF CompilerVersion > 30}
procedure TBaseStore.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>(
    Context,
    procedure(Store : SC)
    begin
      Event(Context, Store);
    end,
    False
  );
end;

procedure TBaseStore.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>; FutureChangesOnly : Boolean);
begin
  RegisterForUpdates<SC>(
    Context,
    procedure(Store : SC)
    begin
      Event(Context, Store);
    end,
    FutureChangesOnly
  );
end;

procedure TBaseStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  RegisterForUpdates<SC>(
    Context,
    Handler,
    False);
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
    Handler(Self);
  finally
    TMonitor.Exit(FViews);
  end;
end;

procedure TBaseStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>; FutureChangesOnly : boolean);
begin
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
    if not FutureChangesOnly then
      Handler(Self);
  finally
    TMonitor.Exit(FViews);
  end;
end;

function TBaseStore.WaitForUpdate<SC>(Handler : TProc<SC>; Timeout : Integer = INFINITE) : boolean;
begin
  if FUpdateEvent.WaitFor(Timeout) = wrSignaled then
  begin
    Handler(Self as SC);
    Result := True;
  end else
    Result := False;
end;

{$ELSE}

procedure TBaseStore.RegisterForUpdates(Context: TObject; Event: TEventMethod<TBaseStore>);
begin
  RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Event(Context, Store);
    end,
    False
  );
end;

procedure TBaseStore.RegisterForUpdates(Context: TObject; Event: TEventMethod<TBaseStore>; FutureChangesOnly : Boolean);
begin
  RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Event(Context, Store);
    end,
    FutureChagnesOnly
  );
end;

procedure TBaseStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseStore>);
begin
  RegisterForUpdates(
    Context,
    Handler,
    False
  );
end;

procedure TBaseStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseStore>; FutureChangesOnly : Boolean);
begin
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
    if not FutureChangesOnly then
      Handler(Self);
  finally
    TMonitor.Exit(FViews);
  end;
end;

function TBaseStore.WaitForUpdate(Handler: TProc<TBaseStore>; Timeout : Integer = INFINITE) : boolean;
begin
  if FUpdateEvent.WaitFor(Timeout) = wrSignaled then
  begin
    Handler(Self);
    Result := True;
  end else
    Result := False;
end;
{$IFEND}

procedure TBaseStore.SafeUpdate(OnUpdate: TProc);
begin
  FCS.TryEnter;
  try
    OnUpdate();
  finally
    FCS.Leave;
  end;
end;

procedure TBaseStore.UnbindAll(Target: TComponent);
var
  i: Integer;
begin
  for i := FBindings.Count-1 downto 0 do
  begin
    if FBindings[i].Target = Target then
    {$IF CompilerVersion > 30}
      FBindings.ExtractAt(i);
    {$ELSE}
      FBindings.Extract(FBindings[i]);
    {$ENDIF}
  end;
end;

procedure TBaseStore.UnregisterForUpdates(Context: TObject);
begin
  TMonitor.Enter(FViews);
  try
    FViews.Remove(Context);
  finally
    TMonitor.Exit(FViews);
  end;
end;

procedure TBaseStore.UpdateBindings;
var
  binding : TBaseStore.IStoreBinding;
begin
  for binding in FBindings do
    binding.UpdateValue;
end;

procedure TBaseStore.AccumulateUpdates;
begin
  DelayedUpdate.&Do;
end;

procedure TBaseStore.UpdateViews;
begin
  DoUpdates;
end;

{ TStores }

class procedure TStores.Bind<SC>(Target: TComponent; const PropertyName: String);
begin
  Bind<SC>(PropertyName, Target, PropertyName, '');
end;

class procedure TStores.Bind<SC>(Target: TComponent; const PropertyName, Session: string);
begin
  Bind<SC>(PropertyName, Target, PropertyName, Session);
end;

class function TStores.Data<SC>: SC;
begin
  Result := Data<SC>('');
end;

class function TStores.Data<SC>(Session: string): SC;
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end;
  bs := st[SC];
  Result := bs as SC;
end;

class procedure TStores.Bind<SC>(const SourceProperty: string; Target: TComponent; const TargetProperty, Session: string);
begin
  WithData<SC>(Session,
    procedure(Store : SC)
    begin
      Store.Bind(SourceProperty, Target, TargetProperty);
    end
  );
end;

class procedure TStores.Bind<SC>(const SourceProperty: string; Target: TComponent; const TargetProperty: String);
begin
  Bind<SC>(SourceProperty, Target, TargetProperty, '');
end;

class constructor TStores.Create;
begin
  FCS := TCriticalSection.Create;
  FStores := TStoreIndex.Create;
end;

class destructor TStores.Destroy;
begin
  FCS.Free;
  FStores.Free;
end;

class procedure TStores.FreeAllStores;
var
  SessionStore: TPair<string, TSessionStores>;
  Session : TPair<TBaseStoreClass, TBaseStore>;
begin
  for SessionStore in FStores do
  begin
    for Session in SessionStore.Value do
    begin
      Session.Value.Free;
    end;
    SessionStore.Value.Free;
  end;
end;

class procedure TStores.Init<SC>(Session: string = '');
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end else
  bs := st[SC];
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Handler: TProc<SC>; FutureChangesOnly : boolean);
begin
  RegisterForUpdates<SC>([''], Context, Handler, FutureChangesOnly);
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Handler: TProc<SC>);
begin
  RegisterForUpdates<SC>([''], Context, Handler);
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Event: TEventMethod<SC>; FutureChangesOnly : Boolean);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Event(Context, Store);
      end,
      FutureChangesOnly
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Event(Context, Store);
      end,
      FutureChangesOnly
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Event: TEventMethod<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Event(Context, Store);
      end
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Event(Context, Store);
      end
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Handler: TProc<SC>; FutureChangesOnly : boolean);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Handler(Store);
      end,
      FutureChangesOnly
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end,
      FutureChangesOnly
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Handler: TProc<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Handler(Store);
      end
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Event: TEventMethod<SC>; FutureChangesOnly : boolean);
begin
  RegisterForUpdates<SC>([Session], Context, Event, FutureChangesOnly);
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>([Session], Context, Event);
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Handler: TProc<SC>; FutureChangesOnly : boolean);
begin
  RegisterForUpdates<SC>([Session], Context, Handler, FutureChangesOnly);
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Handler: TProc<SC>);
begin
  RegisterForUpdates<SC>([Session], Context, Handler);
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>; FutureChangesOnly : boolean);
begin
  RegisterForUpdates<SC>([''], Context, Event, FutureChangesOnly);
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>([''], Context, Event);
end;

class function TStores.Stores(Session: string): TSessionStores;
begin
  FCS.Enter;
  try
    if not FStores.ContainsKey(Session) then
    begin
      Result := TSessionStores.Create;
      FStores.Add(Session, Result);
    end;
    Result := FStores[Session];
  finally
    FCS.Leave;
  end;
end;

class procedure TStores.UnregisterForUpdates<SC>(Session: string; Context: TObject);
begin
  UnregisterForUpdates<SC>([Session], Context);
end;

class procedure TStores.UnregisterForUpdates<SC>(Sessions: TArray<string>; Context: TObject);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if st.ContainsKey(SC) then
    begin
      bs := st[SC];
      bs.UnregisterForUpdates(Context);
    end;
  end;
end;

class procedure TStores.WaitForUpdate<SC>(Session: string;  LastUpdateCount : UInt64; OnData: TDataStoreHandler<SC>; Timeout : Cardinal = TBaseStore.INFINITE);
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end else
    bs := st[SC];
  if bs.UpdateCount = LastUpdateCount then
  begin
   {$If CompilerVersion > 30}
    bs.WaitForUpdate<SC>(
      procedure(Store : SC)
      begin
        OnData(Store);
      end,
      Timeout
    )
    {$ELSE}
    bs.WaitForUpdate(
      procedure(Store : TBaseStore)
      begin
        OnData(Store);
      end,
      Timeout
    )
    {$ENDIF}
  end else
    OnData(bs as SC);
end;

class procedure TStores.WithData<SC>(Session: string; OnData: TDataStoreHandler<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end;
  bs := st[SC];
  OnData(bs as SC);
end;

class procedure TStores.WithData<SC>(OnData: TDataStoreHandler<SC>);
begin
  WithData<SC>('', OnData);
end;

class procedure TStores.UnbindAll<SC>(Target: TComponent);
begin
  UnbindAll<SC>(Target, '');
end;

class procedure TStores.UnbindAll<SC>(Target: TComponent;
  const Session: string);
begin
  WithData<SC>(Session,
    procedure(Store : SC)
    begin
      Store.UnbindAll(Target);
    end
  );
end;

class procedure TStores.UnregisterForUpdates<SC>(Context: TObject);
begin
  UnregisterForUpdates<SC>([''], Context);
end;

class function TStores.Value<SC, T>(OnData: TDataStoreValueHandler<SC, T>) : T;
begin
  Result := Value<SC, T>('', OnData);
end;

class function TStores.Value<SC, T>(Session: string; OnData: TDataStoreValueHandler<SC, T>) : T;
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end;
  bs := st[SC];
  Result := OnData(bs as SC);
end;

{ TBaseDataModuleStore }

constructor TBaseDataModuleStore.Create(AOwner: TComponent);
begin
  inherited;
  FBaseStore := TBaseStore.Create;
end;

destructor TBaseDataModuleStore.Destroy;
begin
  FBaseStore.Free;
  inherited;
end;

procedure TBaseDataModuleStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseDataModuleStore>; FutureChangesOnly : Boolean);
begin
  {$IF CompilerVersion > 30}
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    FutureChangesOnly
  );
  {$ELSE}
  FBaseStore.RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    FutureChangesOnly
  );
  {$ENDIF}
end;

procedure TBaseDataModuleStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseDataModuleStore>);
begin
  {$IF CompilerVersion > 30}
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    False
  );
  {$ELSE}
  FBaseStore.RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    False
  );
  {$ENDIF}
end;

{$IF CompilerVersion > 30}
procedure TBaseDataModuleStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>; FutureChangesOnly : boolean);
begin
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    FutureChangesOnly
  );
end;

procedure TBaseDataModuleStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end,
    False
  );
end;
{$ENDIF}

procedure TBaseDataModuleStore.UnregisterForUpdates(Context: TObject);
begin
  //FBaseStore.UnregisterForUpdates(Context);
end;

procedure TBaseDataModuleStore.UpdateViews;
begin
  FBaseStore.UpdateViews;
end;

{ TBinding }

function TBinding.AsValue: TValue;
begin
  Result := FSourceProp.GetValue(FOwner);
end;

function TBinding.Target: TComponent;
begin
  Result := FTarget;
end;

constructor TBinding.Create(Owner: TBaseStore; Target: TComponent;
  const SourceProperty, TargetProperty: string);
var
  cxt : TRttiContext;
begin
  cxt := TRttiContext.Create;
  FOwner := Owner;
  FTarget := Target;
  FSourceProperty := SourceProperty;
  FTargetProperty := TargetProperty;

  FSourceProp := cxt.GetType(Owner.ClassType).GetProperty(SourceProperty);
  FTargetProp := cxt.GetType(Target.ClassType).GetProperty(TargetProperty);

  if not FTargetProp.IsWritable then
    raise EFluxBindException.Create('Target property "'+TargetProperty+'" is not writable');

  if FSourceProp.IsWritable then
    raise EFluxBindException.Create('Source property "'+SourceProperty+'" must be read only');

  if FTargetProp.PropertyType.TypeKind <> FSourceProp.PropertyType.TypeKind then
    raise EFluxBindException.Create('Types do not match for properties "'+SourceProperty+'" and "'+TargetProperty+'"');

  FIsInstanceProp := FTargetProp.PropertyType.IsInstance;

  if FIsInstanceProp then
  begin
    FSourcePropInstance := FSourceProp.GetValue(FOwner).AsObject;
    FTargetPropInstance := FTargetProp.GetValue(FTarget).AsObject;
  end else
  begin
    FSourcePropInstance := nil;
    FTargetPropInstance := nil;
  end;

end;

function TBinding.SourceProperty: string;
begin
  Result := FSourceProperty;
end;

function TBinding.TargetProperty: string;
begin
  Result := FTargetProperty;
end;

procedure TBinding.UpdateValue;
begin
  if FIsInstanceProp and
    (FSourcePropInstance is TPersistent) and
    (FTargetPropInstance is TPersistent) then
  begin
    TPersistent(FTargetPropInstance).Assign(TPersistent(FSourcePropInstance));
  end else
    FTargetProp.SetValue(FTarget, AsValue);
end;

end.
