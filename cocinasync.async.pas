unit cocinasync.async;

interface

uses System.SysUtils, System.Classes, cocinasync.global, cocinasync.jobs,
  System.SyncObjs;

type
  TExceptionHandler = reference to procedure(E : Exception);

  IMREWSync = interface
     procedure BeginRead;
     procedure EndRead;
     procedure BeginWrite;
     procedure EndWrite;
  end;

  IDelayedDo = interface
    procedure &Do;
    procedure Cancel;
  end;

  TAsync = class(TObject)
  strict private
    FCounter : TThreadCounter;
    FTerminating : boolean;
  strict private
    class var FSynchronizeInMainThread : boolean;
    class var FSync : TCriticalSection;
  public
    class function IsInMainThread : Boolean;
    class function CreateMREWSync : IMREWSync;
    class procedure SynchronizeIfInThread(const proc : TProc);
    class procedure QueueIfInThread(const proc : TProc);
    class property SynchronizeInMainThread : boolean read FSynchronizeInMainThread write FSynchronizeInMainThread;
    class constructor Create;
    class destructor Destroy;

    procedure AfterDo(const After : Cardinal; const &Do : TProc; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil);
    procedure DoLater(const &Do : TProc; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil);
    procedure OnDo(const &On : TFunc<Boolean>; const &Do : TProc; CheckInterval : integer = 1000; &Repeat : boolean = false; SynchronizedOn : boolean = false; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil); overload;
    procedure OnDo(const &On : TFunc<Boolean>; const OnException : TExceptionHandler; const &Do : TProc; const DoException : TExceptionHandler; CheckInterval : integer = 1000; &Repeat : boolean = false; SynchronizedOn : boolean = false; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil); overload;
    procedure OnDo(const &On : TFunc<Boolean>; const &Do : TProc; CheckInterval : integer = 1000; &Repeat : TFunc<boolean> = nil; SynchronizedOn : boolean = false; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil); overload;
    procedure OnDo(const &On : TFunc<Boolean>; const OnException : TExceptionHandler; const &Do : TProc; const DoException : TExceptionHandler; CheckInterval : integer = 1000; &Repeat : TFunc<boolean> = nil; RepeatException : TExceptionHandler = nil; SynchronizedOn : boolean = false; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil); overload;
    function DoEvery(const MS : Cardinal; const &Do : TFunc<Boolean>; SynchronizedDo : boolean = true; OnException : TExceptionHandler = nil) : TThread; overload;
    function DoEvery(const MS : Cardinal; const &Do : TProc; SynchronizedDo : boolean = true; OnException : TExceptionHandler = nil) : TThread; overload;
    function DoEvery(const MS : Cardinal; WithoutDelay : boolean; const &Do : TProc; SynchronizedDo : boolean = true; OnException : TExceptionHandler = nil) : TThread; overload;
    constructor Create; reintroduce; virtual;
    destructor Destroy; override;
    class function AfterLastCall(const WithinMS : Cardinal; OnDo : TProc; SynchronizedDo : boolean = true) : IDelayedDo;
  end;

var
  // NOTE: This global object handles async work at the global level. If you
  //       are using within a form or some other object that could be destroyed
  //       before the app is terminated, then you should create your own instance
  //       in your form or other object and use that instead.  This will ensure
  //       that async operations are complete before your object is destroyed.
  Async : TAsync;

{$IFDEF MSWINDOWS}
  {.$DEFINE USEOURMWERS}
{$ENDIF}

implementation

{$IFDEF USEOURMWERS}uses WinAPI.Windows; {$ENDIF}

type
  TDelayedDo = class(TInterfacedObject, IDelayedDo)
  private
    type
      TDDTimer = class(TThread)
      private
        FEvent : TEvent;
        FMS : Cardinal;
        FSynchronizedDo : Boolean;
        FOnDo : TProc;
        [volatile] FTriggered : boolean;
      protected
        procedure Execute; override;
      public
        constructor Create(MS : Cardinal; const OnDo : TProc; SynchronizedDo : boolean);
        destructor Destroy; override;
        procedure CallDo;
        procedure Cancel;
      end;
  private
    FTimer : TDDTimer;
  public
    constructor Create(WithinMS : Cardinal; const OnDo : TProc; SynchronizedDo : boolean);
    destructor Destroy; override;

    procedure &Do;
    procedure Cancel;
  end;

  // NOTE: The intention here was to provide a faster MREWSync. Unfortunately
  //       the implementation below has a deadlock in it that hasn't yet been fixed
  //       for now the default is to use the MREWSync that comes with Delphi.
  { TFastMREWSync }
  TFastMREWSync = class(TInterfacedObject, IMREWSync)
  private
    FRef : {$IFDEF USEOURMWERS}Integer{$ELSE}TMREWSync{$ENDIF};
    {$IFDEF USEOURMWERS}
    FReadLockCount : Cardinal;
    FWriteLockCount : Cardinal;
    {$ENDIF}
  public
    constructor Create;
    destructor Destroy; override;
    procedure BeginRead;
    procedure EndRead;
    procedure BeginWrite;
    procedure EndWrite;
  end;

{ TDelayedDo }

procedure TDelayedDo.Cancel;
begin
  FTimer.Cancel;
end;

constructor TDelayedDo.Create(WithinMS: Cardinal; const OnDo : TProc; SynchronizedDo : boolean);
begin
  inherited Create;
  FTimer := TDDTimer.Create(WithinMS, OnDo, SynchronizedDo);
end;

destructor TDelayedDo.Destroy;
begin
  FTimer.Terminate;
  FTimer.Cancel;
  inherited;
end;

procedure TDelayedDo.&Do;
begin
  FTimer.CallDo;
end;

{ TDelayedDo.TDDTimer }

procedure TDelayedDo.TDDTimer.CallDo;
begin
  FTriggered := True;
  FEvent.SetEvent;
end;

procedure TDelayedDo.TDDTimer.Cancel;
begin
  FTriggered := False;
end;

constructor TDelayedDo.TDDTimer.Create(MS : Cardinal; const OnDo : TProc; SynchronizedDo : boolean);
begin
  inherited Create(False);
  FEvent := TEvent.Create;
  FSynchronizedDo := SynchronizedDo;
  FMS := MS;
  FOnDo := OnDo;
  FreeOnTerminate := True;
end;

destructor TDelayedDo.TDDTimer.Destroy;
begin
  FEvent.Free;
  inherited;
end;

procedure TDelayedDo.TDDTimer.Execute;
var
  dt : TDateTime;
begin
  while not Terminated do
  begin
    // Wait for a Do to be called.  after 100ms give up waiting and check for terminated again, repeat.
    if FEvent.WaitFor(100) = wrTimeout then
      continue;

    // Wait for FMS. if Do is called within that FMS time, restart the wait.
    repeat
      FEvent.ResetEvent;
    until (FEvent.WaitFor(FMS) = wrTimeOut);

    // Check to see if it was cancelled while waiting. If so, repeat all
    if not FTriggered then
      Continue;

    // Waiting appropriate time so we can finally call ondo
    if FSynchronizedDo then
      TThread.Synchronize(TThread.Current,
        procedure
        begin
          FOnDo();
        end
      )
    else
      FOnDo();
  end;
end;



{ TFastMREWSync }


constructor TFastMREWSync.Create;

begin

  inherited Create;

  {$IFNDEF USEOURMWERS}

  FRef := TMREWSync.Create;

  {$ENDIF}

end;


destructor TFastMREWSync.Destroy;
begin
  {$IFNDEF USEOURMWERS}
  FRef.Free;
  {$ENDIF}

  inherited;
end;

procedure TFastMREWSync.BeginRead;
{$IFDEF USEOURMWERS}
var
  ref : Integer;
begin
  //Wait on writer to reset write flag so Reference.Bit0 must be 0 than increase Reference
  repeat
      ref := Integer(FRef) and not 1;
  until ref = Integer(InterlockedCompareExchange(FRef, ref + 2, ref));
  inc(FReadLockCount);
end;
{$ELSE}
begin
   FRef.BeginRead;
end;
{$ENDIF}

procedure TFastMREWSync.BeginWrite;
{$IFDEF USEOURMWERS}
var
  ref : integer;
begin
  //Wait on writer to reset write flag so omrewReference.Bit0 must be 0 then set omrewReference.Bit0
  repeat
      ref := FRef and (not 1);
  until ref = Integer(InterlockedCompareExchange(FRef, ref+1, ref));

  //Now wait on all readers
  repeat
  until FRef = 1;
  inc(FWriteLockCount);
end;
{$ELSE}
begin
   FRef.BeginWrite;
end;
{$ENDIF}

procedure TFastMREWSync.EndRead;
begin
  {$IFDEF USEOURMWERS}
  //Decrease omrewReference
  InterlockedExchangeAdd(@FRef, -2);
  dec(FReadLockCount);
  {$ELSE}
  FRef.EndRead;
  {$ENDIF}
end;

procedure TFastMREWSync.EndWrite;
begin
  {$IFDEF USEOURMWERS}
  FRef := 0;
  dec(FWriteLockCount);
  {$ELSE}
  FRef.EndWrite;
  {$ENDIF}
end;

{ TAsync }

class function TAsync.AfterLastCall(const WithinMS : Cardinal; OnDo : TProc; SynchronizedDo : boolean = true): IDelayedDo;
begin
  Result := TDelayedDo.Create(WithinMS, OnDo, SynchronizedDo);
end;

constructor TAsync.Create;
begin
  inherited Create;
  FCounter := TThreadCounter.Create;
  FTerminating := False;
end;

class constructor TAsync.Create;
begin
  FSynchronizeInMainThread := True;
  FSync := TCriticalSection.Create;
end;

class function TAsync.CreateMREWSync : IMREWSync;
begin
  Result := TFastMREWSync.Create;
end;


class procedure TAsync.SynchronizeIfInThread(const proc : TProc);
begin
  if FSynchronizeInMainThread then
  begin
    if not IsInMainThread then
    begin
      TThread.Synchronize(TThread.Current,
        procedure
        begin
          proc();
        end
      );
    end else
      proc();
  end else
  begin
    FSync.Enter;
    try
      proc();
    finally
      FSync.Leave;
    end;
  end
end;

class procedure TAsync.QueueIfInThread(const proc : TProc);
begin
  if FSynchronizeInMainThread then
  begin
    if not IsInMainThread then
    begin
      TThread.Queue(TThread.Current,
        procedure
        begin
          proc();
        end
      );
    end else
      proc();
  end else
  begin
    {$IF CompilerVersion >= 32.0}
      TThread.ForceQueue(TThread.Current, TThreadProcedure(proc));
    {$ENDIF}
    {$IF CompilerVersion < 32.0}
    FSync.Enter;
    try
      proc();
    finally
      FSync.Leave;
    end;
    {$ENDIF}
  end
end;

type
  TThreadHack = class(TThread) end;

destructor TAsync.Destroy;
begin
  FTerminating := True;
  FCounter.WaitForAll;
  FCounter.Free;
  inherited;
end;

class destructor TAsync.Destroy;
begin
  FSync.Free;
end;

function TAsync.DoEvery(const MS : Cardinal; const &Do : TFunc<Boolean>; SynchronizedDo : boolean = true; OnException : TExceptionHandler = nil) : TThread;
begin
  Result := TThread.CreateAnonymousThread(
    procedure
    var
      bContinue : boolean;
    begin
      try
        FCounter.NotifyThreadStart;
        try
          bContinue := True;
          while not (TThreadHack(TThread.Current).Terminated or FTerminating) do
          begin
            if TThreadHack(TThread.Current).Terminated or FTerminating then
              Exit;
            sleep(MS);
            if SynchronizedDo then
              SynchronizeIfInThread(
                procedure
                begin
                  bContinue := &Do();
                end
              )
            else
              bContinue := &Do();
            if not bContinue then
              break;
          end;
        finally
          FCounter.NotifyThreadEnd;
        end;
      except
        on E: Exception do
        begin
          if Assigned(OnException) then
            OnException(E);
        end;
      end;
    end
  );
  Result.Start;
end;

function TAsync.DoEvery(const MS: Cardinal; WithoutDelay: boolean;
  const &Do: TProc; SynchronizedDo: boolean;
  OnException: TExceptionHandler): TThread;
begin
  Result := TThread.CreateAnonymousThread(
    procedure
    begin
      try
        FCounter.NotifyThreadStart;
        try
          while not (TThreadHack(TThread.Current).Terminated or FTerminating) do
          begin
            if TThreadHack(TThread.Current).Terminated or FTerminating then
              Exit;
            sleep(MS);
            if WithoutDelay then
            begin
              TJobManager.Execute(
                procedure
                begin
                  if SynchronizedDo then
                    SynchronizeIfInThread(
                      procedure
                      begin
                        &Do();
                      end
                    )
                  else
                    &Do();
                end
              );
            end else
            begin
              if SynchronizedDo then
                SynchronizeIfInThread(
                  procedure
                  begin
                    &Do();
                  end
                )
              else
                &Do();
            end;
          end;
        finally
          FCounter.NotifyThreadEnd;
        end;
      except
        on E: Exception do
        begin
          if Assigned(OnException) then
            OnException(E);
        end;
      end;
    end
  );
  Result.Start;
end;

function TAsync.DoEvery(const MS: Cardinal; const &Do: TProc;
  SynchronizedDo: boolean; OnException: TExceptionHandler): TThread;
begin
  result := DoEvery(MS, False, &Do, SynchronizedDo, OnException);
end;

procedure TAsync.AfterDo(const After : Cardinal; const &Do : TProc; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil);
begin
  TJobManager.Execute(
    procedure
    begin
      if FTerminating then
        Exit;
      FCounter.NotifyThreadStart;
      try
        sleep(After);
        if SynchronizedDo then
          SynchronizeIfInThread(
            procedure
            begin
              &Do();
            end
          )
        else
          &Do();
      finally
        FCounter.NotifyThreadEnd;
      end;
    end,
    JobsOverride, 'TAsync.AfterDo'
  );
end;

procedure TAsync.DoLater(const &Do : TProc; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil);
begin
  TJobManager.Execute(
    procedure
    begin
      if FTerminating then
        Exit;
      FCounter.NotifyThreadStart;
      try
        if SynchronizedDo then
          SynchronizeIfInThread(
            procedure
            begin
              &Do();
            end
          )
        else
          &Do();
      finally
        FCounter.NotifyThreadEnd;
      end;
    end,
    JobsOverride, 'TAsync.DoLater'
  );
end;

class function TAsync.IsInMainThread: Boolean;
begin
  Result := TThread.Current.ThreadID = MainThreadID;
end;

procedure TAsync.OnDo(const &On: TFunc<Boolean>; const &Do: TProc;
  CheckInterval: integer; &Repeat, SynchronizedOn, SynchronizedDo: boolean;
  const JobsOverride: IJobs);
begin
  OnDo(
    &On,
    &Do,
    CheckInterval,
    function : boolean
    begin
      Result := &Repeat;
    end,
    SynchronizedOn,
    SynchronizedDo,
    JobsOverride);
end;

procedure TAsync.OnDo(const &On: TFunc<Boolean>;
  const OnException: TExceptionHandler; const &Do: TProc;
  const DoException: TExceptionHandler; CheckInterval: integer;
  &Repeat: boolean; SynchronizedOn,
  SynchronizedDo: boolean; const JobsOverride: IJobs);
begin
  OnDo(
    &On,
    OnException,
    &Do,
    DoException,
    CheckInterval,
    function : boolean
    begin
      Result := &Repeat;
    end,
    nil,
    SynchronizedOn,
    SynchronizedDo,
    JobsOverride);
end;

procedure TAsync.OnDo(const &On: TFunc<Boolean>; const OnException : TExceptionHandler; const &Do: TProc;
  const DoException: TExceptionHandler; CheckInterval: integer;
  &Repeat: TFunc<boolean>; RepeatException : TExceptionHandler; SynchronizedOn, SynchronizedDo: boolean;
  const JobsOverride: IJobs);
begin
  if not Assigned(&Repeat) then
    &Repeat :=
      function : boolean
      begin
        Result := False;
      end;
  TThread.CreateAnonymousThread(
    procedure
    var
      bOn, bRepeat : Boolean;
    begin
      FCounter.NotifyThreadStart;
      try
        repeat
          if FTerminating then
            Exit;
          repeat
            if FTerminating then
              Exit;
            if SynchronizedOn then
              SynchronizeIfInThread(
                procedure
                begin
                  try
                    bOn := &On();
                  except
                    on E: Exception do
                      if Assigned(&OnException) then
                        &OnException(E);
                  end;
                end
              )
              else
                try
                  bOn := &On();
                except
                  on e: EAbort do
                    exit;
                  on E: Exception do
                    if Assigned(&OnException) then
                      &OnException(E);
                end;
            if bOn then
              break;
            sleep(CheckInterval)
          until bOn;
          if FTerminating then
            Exit;
          if SynchronizedDo then
            SynchronizeIfInThread(
              procedure
              begin
                try
                  &Do();
                except
                  on e: EAbort do
                    exit;
                  on E: Exception do
                    if Assigned(DoException) then
                      DoException(E);
                end;
              end
            )
          else
            try
              &Do();
            except
              on E: Exception do
                if Assigned(DoException) then
                  DoException(E);
            end;
          bRepeat := False;
          try
            bRepeat := &Repeat();
          except
            on E: Exception do
            begin
              if Assigned(RepeatException) then
                RepeatException(E);
              //bRepeat := False;
            end;
          end;
        until not bRepeat;
      finally
        FCounter.NotifyThreadEnd;
      end;
    end
  ).Start;
end;

procedure TAsync.OnDo(const &On : TFunc<Boolean>; const &Do : TProc; CheckInterval : integer = 1000; &Repeat : TFunc<boolean> = nil; SynchronizedOn : boolean = false; SynchronizedDo : boolean = true; const JobsOverride : IJobs = nil);
begin
  OnDo(&On, nil, &Do, nil,CheckInterval, &Repeat, nil, SynchronizedOn, SynchronizedDo, JobsOverride);
end;

initialization
  Async := TAsync.Create;

finalization
  Async.Free;

end.


