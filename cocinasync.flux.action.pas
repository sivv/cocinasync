unit cocinasync.flux.action;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections;

type
  TBaseAction = class;

  EFluxActionException = class(Exception);

  TActionClass = class of TBaseAction;

  TBaseAction = class abstract(TObject)
  strict private
    class var FCS : TMREWSync;
    class var FActionPool : TDictionary<TClass, TQueue<TBaseAction>>;
  strict private
    FSession : string;
  protected
    {$If CompilerVersion > 30}
    class function New<AC : TBaseAction, constructor> : AC; overload;
    class function New<AC : TBaseAction, constructor>(Session : string) : AC; overload;
    class function New(ClassType : TActionClass) : TBaseAction; overload;
    {$ELSE}
    class function New(ClassType : TActionClass) : TBaseAction;
    {$IFEND}
    class procedure Finish(Action : TBaseAction); overload;
    class procedure ClearPools;

    // NOTE on IsIntegral:
    // ----------------------------
    // if many of the same action are sent in a row, should we:
    //    1. execute every one (IsIntegral=True) or
    //    2. only execute the first one, ignoring the rest (IsIntegral=False)
    //
    // This is an optimization for notification style actions which in which responding
    // to each of them would constitute a performance problem (too many repaints for example).
    //
    // If your action must respond to every action instance that is dispatched,
    // then the action class should override this method and set the result to False
    // or descend from TIntegralAction
    //
    function IsIntegral : boolean; virtual; abstract;
    function IsIncidental : boolean;
  public
    property Session : string read FSession;

    procedure Dispatch; reintroduce; overload;

    constructor Create; virtual;
    class constructor Create;
    class destructor Destroy;

    // Finish must be called once an action has been dispatched so that it is
    // returned to the cache for use again the next time an action of the same type
    // is dispatched.
    procedure Finish; overload;
  end;

  TIncidentalAction = class(TBaseAction)
  protected
    function IsIntegral : boolean; override;
  end;

  TIntegralAction = class(TBaseAction)
  protected
    function IsIntegral : boolean; override;
  end;

implementation

uses
  Cocinasync.Flux;

{ TIncidentalAction }

function TIncidentalAction.IsIntegral: boolean;
begin
  Result := False;
end;

{ TIntegralAction }

function TIntegralAction.IsIntegral: boolean;
begin
  Result := True;
end;

{ TBaseAction }

class procedure TBaseAction.ClearPools;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
    end;
  finally
    FCS.EndWrite;
  end;
end;

class constructor TBaseAction.Create;
begin
  FCS := TMREWSync.Create;
  FActionPool := TDictionary<TClass, TQueue<TBaseAction>>.Create;
end;

constructor TBaseAction.Create;
begin
  inherited Create;
end;

class destructor TBaseAction.Destroy;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
      p.Value.Free;
    end;
  finally
    FCS.EndWrite;
  end;
  FCS.Free;
  FActionPool.Free;
end;

procedure TBaseAction.Dispatch;
begin
  Flux.Dispatch(Self);
end;

procedure TBaseAction.Finish;
begin
  Finish(Self);
end;

function TBaseAction.IsIncidental: boolean;
begin
  Result := not IsIntegral;
end;

class function TBaseAction.New(ClassType: TActionClass): TBaseAction;
var
  queue : TQueue<TBaseAction>;
begin
  Result := nil;
  TBaseAction.FCS.BeginRead;
  try
    if TBaseAction.FActionPool.ContainsKey( ClassType ) then
    begin
      queue := TBaseAction.FActionPool[ClassType];
      TMonitor.Enter(queue);
      try
        if queue.Count > 0 then
          Result := queue.Dequeue
      finally
        TMonitor.Exit(queue);
      end;
    end;
  finally
    TBaseAction.FCS.EndRead;
  end;

  if Result = nil then
    Result := ClassType.Create;
end;

{$If CompilerVersion > 30}

class function TBaseAction.New<AC>(Session: string): AC;
var
  queue : TQueue<TBaseAction>;
begin
  Result := nil;

  if Session='*' then
    raise EFluxActionException.Create('Wildcard session actions not supported.');

  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(AC) then
    begin
      queue := FActionPool[AC];
      TMonitor.Enter(queue);
      try
        if queue.Count > 0 then
          Result := AC(queue.Dequeue)
      finally
        TMonitor.Exit(queue);
      end;
    end;
  finally
    FCS.EndRead;
  end;

  if Result = nil then
    Result := AC.Create;

  Result.FSession := Session;
end;

class function TBaseAction.New<AC>: AC;
begin
  Result := New<AC>('');
end;
{$IFEND}

class procedure TBaseAction.Finish(Action: TBaseAction);
var
  queue : TQueue<TBaseAction>;
begin
  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(Action.ClassType) then
    begin
      queue := FActionPool[Action.ClassType];
      TMonitor.Enter(queue);
      try
        queue.Enqueue(Action);
      finally
        TMonitor.Exit(queue);
      end;
    end else
    begin
      FCS.BeginWrite;
      try
        if FActionPool.ContainsKey(Action.ClassType) then
        begin
          Finish(Action);
          exit;
        end;
        queue := TQueue<TBaseAction>.Create;
        queue.Enqueue(Action);
        FActionPool.Add(Action.ClassType, queue);
      finally
        FCS.EndWrite;
      end;
    end;
  finally
    FCS.EndRead;
  end;

end;

end.
