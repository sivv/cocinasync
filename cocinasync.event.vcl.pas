unit cocinasync.event.vcl;

interface

uses
  System.SysUtils,
  System.Classes,
  System.SyncObjs;

type
  TNonBlockingEvent = class(TEvent)
  public
    function WaitFor(Timeout: Cardinal): TWaitResult; override;

    class procedure UnblockPromises;
    class procedure UnblockJobs;

  end;

implementation

uses
  VCL.Forms,
  cocinasync.promise,
  cocinasync.jobs,
  cocinasync.Async;

{ TNonBlockingEvent }

class procedure TNonBlockingEvent.UnblockJobs;
begin
  TJobManager.EventClass := TNonBlockingEvent;
end;

class procedure TNonBlockingEvent.UnblockPromises;
begin
  TPromise.EventClass := TNonBlockingEvent;
end;

function TNonBlockingEvent.WaitFor(Timeout: Cardinal): TWaitResult;
var
  c : Int64;
begin
  if TAsync.IsInMainThread then
  begin
    c := Timeout;
    repeat
      Result := inherited WaitFor(10);
      if Result <> TWaitResult.wrTimeout then
        exit;
      Application.ProcessMessages;
      dec(c,10);
    until c <= 0;
  end else
  begin
    Result := inherited WaitFor(Timeout);
  end;

end;


end.
