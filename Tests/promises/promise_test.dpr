program promise_test;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.Classes,
  promises.test in 'promises.test.pas';

begin
  try
    TestPromise1;
    TestPromise2;
    while not Terminated do
    begin
      CheckSynchronize;
      sleep(10);
      if Terminated then
        break;
    end;
    ReadLn;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
