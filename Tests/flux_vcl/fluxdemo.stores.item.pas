unit fluxdemo.stores.item;

interface

uses
  System.SysUtils,
  System.Classes,
  cocinasync.flux.store,
  System.Generics.Collections;

type
  TDataModule = TBaseDataModuleStore;
  TdmItemStore = class(TDataModule)
  type
    TToDoItem = class(TObject)
      ID : TGuid;
      Text : String;
      Checked : boolean;
      constructor Create(ID : TGUID; Text : String; Checked : Boolean);
    end;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FToDoItems : TList<TToDoItem>;
    function GetCount: integer;
    function GetItem(idx: integer): TTodoItem;
  public
    property Count : integer read GetCount;
    property Items[idx : integer] : TTodoItem read GetItem;
    class function NewID : TGUID;
  end;

var
  dmItemStore: TdmItemStore;

implementation

uses
  cocinasync.flux,
  fluxdemo.Actions;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmItemStore.DataModuleCreate(Sender: TObject);
begin
  FToDoItems := TList<TToDoItem>.Create;

  Flux.Register<TNewItemAction>(Self,
    procedure(Action : TNewItemAction)
    begin
      FToDoItems.Insert(0, TToDoItem.Create(Action.ID, Action.Text, False));
    end
  );

  Flux.Register<TDeleteItemAction>(Self,
    procedure(Action : TDeleteItemAction)
    var
      item : TToDoItem;
    begin
      for item in FToDoItems do
      begin
        if item.ID = Action.ID then
        begin
          FToDoItems.Extract(item);
          item.Free;
          break;
        end;
      end;
    end
  );

  Flux.Register<TToggleItemAction>(Self,
    procedure(Action : TToggleItemAction)
    var
      item : TToDoItem;
    begin
      for item in FToDoItems do
      begin
        if item.ID = Action.ID then
        begin
          item.Checked := Action.Checked;
          break;
        end;
      end;
    end
  );
end;

procedure TdmItemStore.DataModuleDestroy(Sender: TObject);
var
  item: TToDoItem;
begin
  for item in FToDoItems do
  begin
    item.Free;
  end;
  FToDoItems.Free;
end;

function TdmItemStore.GetCount: integer;
begin
  Result := FToDoItems.Count;
end;

function TdmItemStore.GetItem(idx: integer): TTodoItem;
begin
  Result := FToDoItems[idx];
end;

class function TdmItemStore.NewID: TGUID;
begin
  // This helper function should never alter store state.
  // Store state is read only from methods.
  CreateGUid(Result);
end;

{ TDataModule1.TToDoItem }

constructor TdmItemStore.TToDoItem.Create(ID: TGUID; Text: String;
  Checked: Boolean);
begin
  inherited Create;
  Self.ID := ID;
  Self.Text := Text;
  Self.Checked := Checked;
end;

end.
