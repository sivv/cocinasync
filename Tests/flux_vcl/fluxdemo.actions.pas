unit fluxdemo.actions;

interface

uses
  System.SysUtils,
  System.Classes,
  cocinasync.flux.action;

type
  TNewItemAction = class(TIntegralAction)
  private
    FText: string;
    FID: TGUID;
  public
    function ToString: string; override;
    property Text : string read FText;
    property ID : TGUID read FID;
    class procedure Send(ID : TGuid; Text : string);
  end;

  TDeleteItemAction = class(TIntegralAction)
  private
    FID: TGUID;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    class procedure Send(ID : TGuid);
  end;

  TToggleItemAction = class(TIntegralAction)
  private
    FID: TGUID;
    FChecked: Boolean;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    property Checked : Boolean read FChecked;
    class procedure Send(ID : TGuid; Checked : boolean);
  end;

  TToggleHistoryAction = class(TIntegralAction)
  public
    class procedure Send;
  end;

implementation

{ TNewItemAction }

class procedure TNewItemAction.Send(ID: TGuid; Text: string);
var
  Result : TNewItemAction;
begin
  Result := inherited New<TNewItemAction>;
  Result.FID := ID;
  Result.FText := Text;
  Result.Dispatch;
end;

function TNewItemAction.ToString: string;
begin
  Result := 'Added Item ('+GuidToString(FID)+': '+FText+')';
end;

{ TDeleteItemAction }

class procedure TDeleteItemAction.Send(ID: TGuid);
var
  Result : TDeleteItemAction;
begin
  Result := inherited New<TDeleteItemAction>;
  Result.FID := ID;
  Result.Dispatch;
end;

function TDeleteItemAction.ToString: string;
begin
  Result := 'Deleted Item ('+GuidToString(FID)+')';
end;

{ TToggleItemAction }

class procedure TToggleItemAction.Send(ID: TGuid; Checked: boolean);
var
  Result : TToggleItemAction;
begin
  Result := inherited New<TToggleItemAction>;
  Result.FID := ID;
  Result.FChecked := Checked;
  Result.Dispatch;
end;

function TToggleItemAction.ToString: string;
begin
  if FChecked then
    Result := 'Marked Finished ('+GuidToString(FID)+')'
  else
    Result := 'Marked Unfinished ('+GuidToString(FID)+')';
end;

{ TToggleHistoryAction }

class procedure TToggleHistoryAction.Send;
begin
  (inherited New<TToggleHistoryAction>).Dispatch;
end;

end.
