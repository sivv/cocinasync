unit fluxdemo.vcl.history;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls;

type
  TfrmHistory = class(TForm)
    lbHistory: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  end;

var
  frmHistory: TfrmHistory;

implementation

uses
  cocinasync.flux,
  cocinasync.flux.view.vcl,
  fluxdemo.actions,
  fluxdemo.stores.history;

{$R *.dfm}

procedure TfrmHistory.FormCreate(Sender: TObject);
begin
  Stores.Bind<THistoryStore>(lbHistory, 'Items');
end;

procedure TfrmHistory.FormDestroy(Sender: TObject);
begin
  Stores.UnbindAll<THistoryStore>(lbHistory);
end;

initialization
  TViews.On<TToggleHistoryAction>.&Do(
    procedure
    begin
      if frmHistory.Visible then
        frmHistory.Hide
      else
        frmHistory.Show;
    end
  ).ThenNothing;

end.
