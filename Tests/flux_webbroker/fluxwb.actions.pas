unit fluxwb.actions;

interface

uses
  System.SysUtils,
  System.Classes,
  Cocinasync.flux.action;

type
  TNewItemAction = class(TIntregalAction)
  private
    FText: string;
    FID: TGUID;
  public
    function ToString: string; override;
    property Text : string read FText;
    property ID : TGUID read FID;
    class function Send(Session : string; ID : TGuid; Text : string) : TNewItemAction;
  end;

  TDeleteItemAction = class(TIntregalAction)
  private
    FID: TGUID;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    class function Send(Session : string; ID : TGuid) : TDeleteItemAction;
  end;

  TToggleItemAction = class(TIntregalAction)
  private
    FID: TGUID;
    FChecked: Boolean;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    property Checked : Boolean read FChecked;
    class function Send(Session : string; ID : TGuid; Checked : boolean) : TToggleItemAction;
  end;


implementation

{ TNewItemAction }

class function TNewItemAction.Send(Session: string; ID: TGuid;
  Text: string): TNewItemAction;
begin
  Result := inherited New<TNewItemAction>(Session);
  Result.FID := ID;
  Result.FText := Text;
  Result.Dispatch;
end;

function TNewItemAction.ToString: string;
begin
  Result := 'Added Item ('+GuidToString(FID)+': '+FText+') on session "'+Self.Session+'"';
end;

{ TDeleteItemAction }

class function TDeleteItemAction.Send(Session: string;
  ID: TGuid): TDeleteItemAction;
begin
  Result := inherited New<TDeleteItemAction>(Session);
  Result.FID := ID;
  Result.Dispatch;
end;

function TDeleteItemAction.ToString: string;
begin
  Result := 'Deleted Item ('+GuidToString(FID)+') on session "'+Self.Session+'"';
end;

{ TToggleItemAction }

class function TToggleItemAction.Send(Session: string; ID: TGuid;
  Checked: boolean): TToggleItemAction;
begin
  Result := inherited New<TToggleItemAction>(Session);
  Result.FID := ID;
  Result.FChecked := Checked;
  Result.Dispatch;
end;

function TToggleItemAction.ToString: string;
begin
  if FChecked then
    Result := 'Marked Finished ('+GuidToString(FID)+') on session "'+Self.Session+'"'
  else
    Result := 'Marked Unfinished ('+GuidToString(FID)+') on session "'+Self.Session+'"';
end;

end.
