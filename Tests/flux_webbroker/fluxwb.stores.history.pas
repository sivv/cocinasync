unit fluxwb.stores.history;

interface

uses System.Classes, System.SysUtils, cocinasync.flux.store;

type

  THistoryStore = class(TBaseStore)
  private
    FItems: TStrings;
    procedure LogAction(Action : TObject);
  public
    constructor Create; override;
    destructor Destroy; override;

    property Items : TStrings read FItems;
  end;


implementation

uses
  cocinasync.flux, fluxwb.actions;

{ THistoryStore }

constructor THistoryStore.Create;
begin
  inherited;
  FItems := TStringList.Create;

  Flux.Register<TNewItemAction>(Session, Self,
    procedure(Action : TNewItemAction)
    begin
      LogAction(Action);
    end
  );

  Flux.Register<TDeleteItemAction>(Session, Self,
    procedure(Action : TDeleteItemAction)
    begin
      LogAction(Action);
    end
  );

  Flux.Register<TToggleItemAction>(Session, Self,
    procedure(Action : TToggleItemAction)
    begin
      LogAction(Action);
    end
  );
end;

destructor THistoryStore.Destroy;
begin
  FItems.Free;
  inherited;
end;

procedure THistoryStore.LogAction(Action : TObject);
var
  sDate : string;
begin
  DateTimeToString(sDate, 'yyyy-mm-dd hh:nn:ss', Now);
  FItems.Add(sDate+' '+Action.ToString);
end;

end.
