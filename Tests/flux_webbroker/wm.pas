unit wm;

interface

uses
  System.SysUtils,
  System.Classes,
  Web.HTTPApp,
  chimera.json;

type
  TWeb = class(TWebModule)
    procedure WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebDefaultHandlerAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleAfterDispatch(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebactLogAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebactListTodoAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebactAddTodoAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebactToggleTodoAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
  private
    FCurrentSession : string;
    FPostData : IJSONObject;
    FOutData : IJSONObject;
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWeb;

implementation

uses
  fluxwb.actions,
  fluxwb.Stores.item,
  fluxwb.Stores.history,
  cocinasync.flux,
  System.Hash,
  System.NetEncoding,
  System.IOUtils;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TWeb.WebModuleAfterDispatch(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
begin
  FCurrentSession := '';
  FPostData := nil;
  if Assigned(FOutData) then
  begin
    Response.StatusCode := 200;
    Response.ContentType := 'application/json';
    Response.Content := FOutData.AsJSON;
    FOutData := nil;
  end else if Response.ContentStream = nil then
  begin
    Response.StatusCode := 500;
    Response.Content := 'No Response Sent';
  end;
end;

procedure TWeb.WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  sSession : string;
  g : TGUID;
  c : TCookie;
  sl : TStringList;
begin
  if (Request.CookieFields.IndexOfName('_session_') < 0) or (Request.CookieFields.Values['_session_'] = '') then
  begin
    CreateGuid(g); // NOTE: Guids aren't really safe for session IDs in real life applications
    sSession := g.ToString;
  end else
    sSession := TNetEncoding.URL.Decode(Request.CookieFields.Values['_session_']);

  c := Response.Cookies.Add;
  c.Name := '_session_';
  c.Value := sSession;
  c.Expires := Now+100;

  FCurrentSession := sSession;
  if Request.MethodType = mtPost then
    FPostData := TJSON.From(Request.Content)
  else
    FPostData := nil;
  FOutData := nil;
end;

procedure TWeb.WebactAddTodoAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  g : TGUID;
begin
  if Assigned(FPostData) then
  begin
    CreateGuid(g);
    TNewItemAction.Send(FCurrentSession, g, FPostData.Strings['text']);
    FOutData := TJSON.New;
    FOutData.GUIDs['id'] := g;
    FOutData.Strings['text'] := FPostData.Strings['text'];
  end;
end;

procedure TWeb.WebactListTodoAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  jsa : IJSONArray;
  iUpdateCount : UINt64;
begin
  if Assigned(FPostData) then
  begin
    iUpdateCount := FPostData.Integers['update_count'];
    jsa := TJSONArray.New;
    try
      Stores.WaitForUpdate<TItemStore>(FCurrentSession, iUpdateCount,
        procedure(Store : TItemStore)
        var
          jso : IJSONObject;
          i : integer;
          item : TItemStore.TToDoItem;
        begin
          for i := 0 to Store.Count-1 do
          begin
            item := Store.Items[i];
            jso := TJSON.New;
            jso.GUIDs['id'] := item.ID;
            jso.Strings['text'] := item.Text;
            jso.Booleans['checked'] := item.Checked;
            jsa.Add(jso);
          end;
          iUpdateCount := Store.UpdateCount;
        end,
        30000
      );
    finally
      FOutData := TJSON.New;
      FOutData.Arrays['items'] := jsa;
      FOutData.Integers['update_count'] := iUpdateCount;
    end;
  end;
end;

procedure TWeb.WebactLogAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  jsa : IJSONArray;
  iUpdateCount : UINt64;
begin
  if Assigned(FPostData) then
  begin
    iUpdateCount := FPostData.Integers['update_count'];
    jsa := TJSONArray.New;
    try
      Stores.WaitForUpdate<THistoryStore>(FCurrentSession, iUpdateCount,
        procedure(Store : THistoryStore)
        var
          s : string;
        begin
          for s in Store.Items do
          begin
            jsa.Add(s);
          end;
          iUpdateCount := Store.UpdateCount;
        end,
        30000
      );
    finally
      FOutData := TJSON.New;
      FOutData.Arrays['items'] := jsa;
      FOutData.Integers['update_count'] := iUpdateCount;
    end;
  end;
end;

procedure TWeb.WebactToggleTodoAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
begin
  if Assigned(FPostData) then
  begin
    TToggleItemAction.Send(FCurrentSession, FPostData.GUIDs['id'], FPostData.Booleans['checked']);
    FOutData := FPostData;
  end;
end;

procedure TWeb.WebDefaultHandlerAction(Sender: TObject; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  fs : TFileStream;
begin
  Response.ContentType := 'text\html';
  fs := TFileStream.Create(TPath.Combine(ExtractFilePath(ParamStr(0)), '..\..\todo.html'), fmOpenRead or fmShareDenyNone);
  Response.ContentStream := fs;
end;


end.
